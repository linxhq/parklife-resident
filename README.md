## Development environment

*  Node version : v10.16.1
*  React Native version : react-native-cli: 2.0.1
*  Xcode and Android Studio

## Language environment

*  React Native 0.59.9
*  TypeScript
*  Integrated Redux, React Redux and structor of Redux for project.
*  Redux form, Redux Persist.

## Files definition

*  example.tsx is file can write components of React Native
*  example.ts is file can't write components of React Native

## How to run an application in a development environment

1/ Clone Repo : `https://gitlab.com/linxhq/parklife-resident.git`

2/ Goto repo on terminal and run `yarn install` or `npm install`

3/ Run App for IOS : `react-native run-ios` or go to folder with Path : `Project/ios/` and run file `ParklifeResident.xcodeproj`

[*] Require : OS is MacOS.

4/ Run App for Android : `react-native run-android` or open Android Studio and click `Open an existing Android Studio Project` -> choose folder `Project/android/`

[*] Runs well on all operating systems.

### Feel !!!!

## Project Structor

```
assets
    fonts/               fonts for Apps
    images/              images for Apps
src
    Actions/             for Redux
    Components/          all components for Apps
    Configs/             Interfaces, Environment, StoreConfig: for Redux
    Models/              Manager Models for Apps
    Reducers/            for Redux
    Screens/             all screens for Apps
    Services/            Manager action comunication with backend
    Utils/               HeaderStyles, showErrors, Styles
    Main.tsx             Navigate Login screen and Routes
    Routes.tsx           Navigate all screen after Login
App.tsx                  root for Apps
package.json             all dependencies for Apps
```





