import React, {Component} from 'react';
import {StyleSheet} from 'react-native';

// import screen Main
import Main from './src/Main';

interface Props {}
export default class App extends Component<Props> {
  render() {
    return (
      <Main />
    );
  }
}

const styles = StyleSheet.create({

});