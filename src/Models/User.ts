export type User = {
    _id: string;
    uuid: string;
    email: string;
    email_verified: boolean;
    status: UserStatus;
    full_name?: string;
    avatar: string;
    created_at?: string;
    updated_at?: string;
};

enum UserStatus {
  Active = 'Active',
  Inactive = 'Inactive',
}
