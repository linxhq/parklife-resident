import {
    USER_LOGIN
} from '../Actions/userActions';
import { User } from '../Models/User';
  
const initialState = {
    data: {} as any
};
  
export interface UserState {
    data: User;
}
  
export default function userReducer(state: UserState = initialState, action) {
    switch (action.type) {
      case USER_LOGIN: {
        console.log(USER_LOGIN);
        return { ...state, loading: true };
      }
      default: {
        return state;
      }
    }
}
  