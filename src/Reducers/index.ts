import userReducer, { UserState } from './userReducers';

export const rootReducers = {
  user: userReducer
};

export interface RootState {
  user: UserState;
}

export type UserState = UserState;
