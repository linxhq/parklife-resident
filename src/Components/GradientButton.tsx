import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { Text, View, TouchableOpacity, StyleSheet, ViewStyle, TextStyle, Platform, Image } from 'react-native';
import { normalizeW, normalizeH, Font, Color } from '../Utils/Styles/styles';

const styles = StyleSheet.create({
  gradientButton: {
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: '#EFEFEF',
    padding: 12,
    margin: 1,
    minWidth: 217,
    backgroundColor: '#272727',
    borderRadius: 2,
  },
});

interface Props {
  onPress(): void;
  text: string;
  containerStyle?: ViewStyle;
  textStyle?: TextStyle;
}

export const GradientButton = (props: Props) => (
  <LinearGradient
    // colors={['#00FFFF', '#17C8FF', '#329BFF', '#4C64FF', '#6536FF', '#8000FF']}
    colors={['#1be091', '#01aac3']}
    start={{ x: 0.0, y: 1.0 }}
    end={{ x: 1.0, y: 1.0 }}
    style={[styles.gradientButton, { ...props.containerStyle }]}
  >
    <TouchableOpacity onPress={() => props.onPress()} activeOpacity={0.9} style={styles.buttonContainer}>
      <Text style={[styles.buttonText, { ...props.textStyle }]}>{props.text}</Text>
    </TouchableOpacity>
  </LinearGradient>
);

export const GradientButtonFill = (props: { icon; text; onPress; active }) => (
  <LinearGradient
    colors={props.active ? ['#1be091', '#01aac3'] : ['transparent', 'transparent']}
    start={{ x: 0.0, y: 1.0 }}
    end={{ x: 1.0, y: 1.0 }}
    style={{
      flex: 1,
      padding: normalizeH(11),
      borderWidth: 2,
      borderColor: Color.pureWhite,
      borderRadius: 5,
    }}
  >
    <TouchableOpacity onPress={() => props.onPress()} activeOpacity={0.9} style={{ alignItems: 'center' }}>
      <Image source={props.icon} style={{ width: normalizeW(40), height: normalizeH(30), resizeMode: 'contain' }} />
      <Text
        style={{
          marginTop: normalizeH(4),
          fontFamily: Font.regular,
          fontSize: normalizeW(17),
          color: Color.pureWhite,
        }}
      >
        {props.text}
      </Text>
    </TouchableOpacity>
  </LinearGradient>
);

interface TransProps {
  onPress(): void;
  text?: string;
  containerStyle?: ViewStyle;
  textStyle?: TextStyle;
  width: any;
  height: number;
  borderWidth: number;
  colors?: string[];
  children?: any;
}

export const GradientButtonTransparent = (props: TransProps) => {
  const { width, height, borderWidth } = props;
  const gradients = props.colors || ['#1be091', '#01aac3'];

  return (
    <View
      style={[
        props.containerStyle,
        {
          width,
          height,
          shadowColor: 'white',
          shadowOffset: {
            width: 0,
            height: 0,
          },
          shadowOpacity: 0.5,
          shadowRadius: 5,
        },
      ]}
    >
      <LinearGradient
        colors={gradients}
        start={{ x: 0.0, y: 1.0 }}
        end={{ x: 1.0, y: 1.0 }}
        style={{
          height: borderWidth,
          overflow: 'hidden',
          borderTopLeftRadius: borderWidth,
          borderTopRightRadius: borderWidth,
        }}
      />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: -1,
          marginBottom: -1,
        }}
      >
        <View style={{ width: borderWidth, overflow: 'hidden' }}>
          <LinearGradient
            colors={gradients}
            start={{ x: 0.0, y: 1.0 }}
            end={{ x: 1.0, y: 1.0 }}
            style={{
              height: Platform.OS === 'ios' ? height : height - 4,
              width: Platform.OS === 'ios' ? width : borderWidth,
            }}
          />
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => props.onPress()}
          style={{
            width: '90%',
            height: '90%',
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {props.children ? props.children : <Text style={props.textStyle}>{props.text}</Text>}
        </TouchableOpacity>

        <View
          style={{
            width: borderWidth,
            overflow: 'hidden',
            flexDirection: 'row-reverse',
          }}
        >
          <LinearGradient
            colors={gradients}
            start={{ x: 0.0, y: 1.0 }}
            end={{ x: 1.0, y: 1.0 }}
            style={{
              height: Platform.OS === 'ios' ? height : height - 4,
              width: Platform.OS === 'ios' ? width : borderWidth,
            }}
          />
        </View>
      </View>
      <LinearGradient
        colors={gradients}
        start={{ x: 0.0, y: 1.0 }}
        end={{ x: 1.0, y: 1.0 }}
        style={{
          height: borderWidth,
          overflow: 'hidden',
          borderBottomLeftRadius: borderWidth,
          borderBottomRightRadius: borderWidth,
        }}
      />
    </View>
  );
};
