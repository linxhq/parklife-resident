export default {
    headerStyle: {
      backgroundColor: "transparent"
    },
    headerTitleStyle: {
      fontWeight: "bold",
      color: "black",
      fontSize: 18,
      zIndex: 1,
      lineHeight: 23
    },
    headerTintColor: "#fff"
};