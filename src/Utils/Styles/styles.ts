import { Dimensions, Platform, PixelRatio, StyleSheet } from 'react-native';

// COLOR APPLICATION
export const ColorApp = {
    purple: '#4F2A55',
    black: '#333333',
    white: '#EFEFEF',
};
// END COLOR APPLICATION

// BACKGROUND APPLICATION
export const BackgroundApp = {
    purple: '#D5D4BD',
    black: '#333333',
    white: '#EFEFEF',
};
// END BACKGROUND APPLICATION

// FONT APPLICATION
export const FontApp = {
    black: 'Rubik-Black',
    blackBlackItalic: 'Rubik-BlackItalic',
    bold: 'Rubik-Bold',
    boldItalic: 'Rubik-BoldItalic',
    italic: 'Rubik-Italic',
    light: 'Rubik-Light',
    lightItalic: 'Rubik-LightItalic',
    medium: 'Rubik-Medium',
    mediumItalic: 'Rubik-MediumItalic',
    regular: 'Rubik-Regular',
};
// END FONT APPLICATION

// FONT SIZE TEXT APPLICATION
export const FontSizeTextApp = {
    textSmall: 14,
    textMedium: 18,
    textLarge: 22,
    textSuperLarge: 26,
};
// END FONT SIZE TEXT APPLICATION

export const Color = {
  turquoise: '#01a9c3',
  black: '#333333',
  white: '#EFEFEF',
  pureWhite: '#FFFFFF',
  darkslategray: '#2c2c2c',
  gray: 'gray',
  gold: '#eba12d',
};

export const Font = {
  regular: 'ProximaNova-Regular',
  bold: 'ProximaNova-Bold',
  semibold: 'ProximaNova-Semibold',
  light: 'ProximaNova-Light',
};

export function normalizeW(val) {
  // normalize based on width
  const ratio = val / 375; // 375 is based width of iphone 7
  const newVal = Math.round(ratio * Dimensions.get('window').width);
  return newVal;
}

export function normalizeH(val) {
  // normalize based on height
  const ratio = val / 736; // 736 is based height of iphone 7
  const newVal = Math.round(ratio * Dimensions.get('window').height);
  return newVal;
}

const parseAlign = key => {
  switch (key) {
    case 'start':
      return 'flex-start';
    case 'end':
      return 'flex-end';
    case 'between':
      return 'space-between';
    case 'around':
      return 'space-around';
    default:
      return key;
  }
};

const row: any = (just, align) => ({
  flexDirection: 'row',
  justifyContent: parseAlign(just),
  ...(align ? { alignItems: parseAlign(align) } : {}),
});

const col: any = (just, align) => ({
  flexDirection: 'column',
  justifyContent: parseAlign(just),
  ...(align ? { alignItems: parseAlign(align) } : {}),
});

export const appStyles = StyleSheet.create({
  rowBetween: {
    ...row('between'),
  },
  colBetween: {
    ...col('between'),
  },
  rowBetweenCenter: {
    ...row('between', 'center'),
  },
  colBetweenCenter: {
    ...col('between', 'center'),
  },
  rowBetweenStart: {
    ...row('between', 'start'),
  },
  colBetweenStart: {
    ...col('between', 'start'),
  },
});
