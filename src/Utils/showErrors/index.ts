import React from 'react';
import { ToastAndroid, Platform } from 'react-native';

export function showErrorMessage(message) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, 5000);
  } else {
    alert(message);
  }
}

export function showInfoMessage(message) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, 5000);
  } else {
    alert(message);
  }
}

export function showBarMessage(message) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, 5000);
  }
}

export function pad(val, size = 2) {
  let s = String(val);
  while (s.length < (size || 2)) {
    s = '0' + s;
  }
  return s;
}
