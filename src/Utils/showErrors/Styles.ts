import { Dimensions, Platform, PixelRatio, StyleSheet } from 'react-native';

export const Color = {
  turquoise: '#01a9c3',
  black: '#333333',
  white: '#EFEFEF',
  pureWhite: '#FFFFFF',
  darkslategray: '#2c2c2c',
  gray: 'gray',
  gold: '#eba12d',
};

export const Font = {
  regular: 'ProximaNova-Regular',
  bold: 'ProximaNova-Bold',
  semibold: 'ProximaNova-Semibold',
  light: 'ProximaNova-Light',
};

export function normalizeW(val) {
  // normalize based on width
  const ratio = val / 375; // 375 is based width of iphone 7
  const newVal = Math.round(ratio * Dimensions.get('window').width);
  return newVal;
}

export function normalizeH(val) {
  // normalize based on height
  const ratio = val / 736; // 736 is based height of iphone 7
  const newVal = Math.round(ratio * Dimensions.get('window').height);
  return newVal;
}

const parseAlign = key => {
  switch (key) {
    case 'start':
      return 'flex-start';
    case 'end':
      return 'flex-end';
    case 'between':
      return 'space-between';
    case 'around':
      return 'space-around';
    default:
      return key;
  }
};

const row: any = (just, align) => ({
  flexDirection: 'row',
  justifyContent: parseAlign(just),
  ...(align ? { alignItems: parseAlign(align) } : {}),
});

const col: any = (just, align) => ({
  flexDirection: 'column',
  justifyContent: parseAlign(just),
  ...(align ? { alignItems: parseAlign(align) } : {}),
});

export const appStyles = StyleSheet.create({
  rowBetween: {
    ...row('between'),
  },
  colBetween: {
    ...col('between'),
  },
  rowBetweenCenter: {
    ...row('between', 'center'),
  },
  colBetweenCenter: {
    ...col('between', 'center'),
  },
  rowBetweenStart: {
    ...row('between', 'start'),
  },
  colBetweenStart: {
    ...col('between', 'start'),
  },
});
