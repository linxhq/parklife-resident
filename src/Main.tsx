import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store } from './Configs/StoreConfig';
import RoutesStack from './Routes';
import LoginScreen from './Screens/Login/Login';
import LoginDarkScreen from './Screens/Login/LoginDark';
import { createStackNavigator, createAppContainer } from "react-navigation";
import { setCustomTextInput, setCustomText } from "react-native-global-props";

const customTextInputProps = {
    style: {
      fontFamily: "Rubik-Light"
    }
};
// Setting default styles for all Text components.
const customTextProps = {
    style: {
      fontFamily: "Rubik-Regular"
    }
};
  
setCustomTextInput(customTextInputProps);
setCustomText(customTextProps);

const LoginStack = createStackNavigator({
    Login: LoginScreen,
    LoginDark: LoginDarkScreen,
}, {
    headerMode: 'none',
});

const AppNavigator = createStackNavigator(
    {
        Login: { screen: LoginStack },
        Routes: { screen: RoutesStack }

    },
    {
      initialRouteName: "Login",
      headerMode: "none",
      navigationOptions: {
        header: null
      }
    }
);

const AppContainer = createAppContainer(AppNavigator);

export default class Main extends Component {
    render() {
        console.disableYellowBox = true;
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        )
    }
}
