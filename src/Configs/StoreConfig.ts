import { createStore, combineReducers, Store } from 'redux';
import { rootReducers, RootState } from '../Reducers';

export const store: Store<RootState> = createStore(
  combineReducers({
    ...rootReducers,
  })
);

