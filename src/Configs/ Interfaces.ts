import { NavigationScreenProp } from 'react-navigation';
import { NavigationStackScreenOptions, NavigationScreenProps, NavigationScreenConfigProps } from 'react-navigation';

export interface ComponentProps extends NavigationScreenProps<any> {}

export type NavigationOptions = (screenConfigProps: NavigationScreenConfigProps) => NavigationStackScreenOptions;
