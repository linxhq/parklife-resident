import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Text, ScrollView, Image, SafeAreaView, Dimensions, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Entypo';
import ImageSlider from 'react-native-image-slider';
import { Container, Header, Item, Input, Icon, Button } from 'native-base';
import { SearchBar } from 'react-native-elements';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

class LayoutSearchHeader extends React.Component {
    state = {
        search: '',
    };
    updateSearch = search => {
        this.setState({ search });
    };
    render() {
        const { search } = this.state;
        return (
            <SearchBar
                placeholder="Search..."
                onChangeText={this.updateSearch}
                value={search}
                containerStyle={{ width: '100%', backgroundColor: 'white', borderTopWidth: 0 }}
                inputContainerStyle={{ height: '100%' }}
                inputStyle={{ color: 'white', fontSize: 15 }}
                lightTheme={true}
                round={false}
            />
        );
    }
}

interface State {
    showRealApp: boolean;
}

interface Props extends ComponentProps {
    
}

class FlastlistResidentialMarket extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => ({
        // title: 'Search',
        headerTitle: <LayoutSearchHeader />,
        headerRight: <TouchableOpacity onPress={() => navigation.navigate('RegisterProduct')}><Ionicons name="plus" size={30} color="#177EFB" style={{ marginRight: 15 }} /></TouchableOpacity>,
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });

    

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                {/* <View style={{ width: '100%', height: 150, marginRight: 10 }}> */}
                                    <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/son-mong-tay.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>30000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/dem-lau-chan.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Đệm lau chân</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>20000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/son-mong-tay.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>30000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/dem-lau-chan.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Đệm lau chân</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>20000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/son-mong-tay.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>30000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/dem-lau-chan.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Đệm lau chân</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>20000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/son-mong-tay.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>30000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/dem-lau-chan.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Đệm lau chân</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>20000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/son-mong-tay.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>30000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailFlastlistResidentialMarket')}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/dem-lau-chan.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Đệm lau chân</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>20000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    
                                {/* </View> */}
                                
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: normalizeH(300),
        resizeMode: 'cover'
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    title: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
    },
});

export default FlastlistResidentialMarket;