//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, TextInput, Platform } from 'react-native';
import IoniconsMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import IoniconsEntypo from 'react-native-vector-icons/Entypo';


export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
  }
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

class TextRegisterProductTitle extends React.Component {
    render() {
      return (
        <View><Text>Đăng ký sản phẩm mới</Text></View>
      );
    }
}

// create a component
class RegisterProduct extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Đăng ký sản phẩm mới'
        // headerTitle: <TextRegisterProductTitle />,
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#F1F1F7'}}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{width: '100%', height: normalizeW(100), padding: 10}}>
                        <View style={{backgroundColor: 'white', width: '100%', height: '100%', padding: 10}}>
                            <Text style={{fontSize: 13}}>Loại thông tin</Text>
                            <View style={{ bottom: 0, right: 0, position: 'absolute', padding: 10 }}>
                                <Text style={{fontSize: 13, borderBottomWidth: 0.5}}>Chọn loại thông tin</Text>
                                <View style={{borderWidth: 0.5}}></View>
                            </View>
                        </View>
                    </View>
                    <View style={{width: '100%', height: 230, padding: 10}}>
                        <View style={{backgroundColor: 'white', width: '100%', height: '100%', padding: 10}}>
                            <Text style={{fontSize: 14}}>Thông tin sản phẩm</Text>
                            <Text style={{fontSize: 13, marginTop: 5}}>Mô tả sản phẩm</Text>
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                style={styles.input}
                            />
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                style={styles.input}
                            />
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                style={styles.input}
                            />
                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                style={styles.input}
                            />
                        </View>
                    </View>
                    <View style={{width: '100%', height: normalizeW(120), padding: 10}}>
                        <View style={{backgroundColor: 'white', width: '100%', height: '100%', padding: 10}}>
                            <Text style={{fontSize: 14}}>Tải ảnh sản phẩm</Text>
                            <TouchableOpacity>
                                <IoniconsEntypo name="camera" size={30} color="#177EFB" style={{ marginTop: 15 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{width: '100%', height: normalizeW(100), padding: 10}}>
                        <View style={{backgroundColor: 'white', width: '100%', height: '100%', padding: 10}}>
                            <Text style={{fontSize: 13}}>Giá sản phẩm</Text>
                            <View style={{ bottom: 0, right: 0, position: 'absolute', padding: 10, width: '50%'}}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '50%', height: 20}}>
                                        <TextInput
                                            underlineColorAndroid="transparent"
                                            placeholderTextColor="black"
                                            style={styles.inputPrice}
                                        />
                                    </View>
                                    <View style={{width: '50%', height: 20, padding: 2}}>
                                        <Text style={{fontSize: 13}}>VND</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{width: '100%', height: 230, padding: 10}}>
                        <View style={{backgroundColor: 'white', width: '100%', height: '100%', padding: 10}}>
                            <View style={{backgroundColor: 'white', width: '100%', height: '100%', padding: 10}}>
                                <Text style={{fontSize: 14}}>Thông tin liên hệ</Text>
                                <TextInput
                                    placeholder="SĐT :"
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="black"
                                    style={styles.input}
                                />
                                <TextInput
                                    placeholder="Địa chỉ :"
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="black"
                                    style={styles.input}
                                />
                                <TextInput
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="black"
                                    style={styles.input}
                                />
                                <TextInput
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor="black"
                                    style={styles.input}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={{width: '100%', height: normalizeH(100), padding: 10}}>
                        <View style={{width: '100%', height: '100%'}}>
                            <TouchableOpacity
                                style={{
                                    backgroundColor: '#28F362', 
                                    width: '30%', 
                                    height: '50%', 
                                    justifyContent: 'center', 
                                    alignItems: 'center',
                                    borderRadius: 4, 
                                }}>
                                <View>
                                        <Text style={{fontSize: 13, color: 'white'}}>Gửi</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        padding: 10,
        flexDirection: 'row',
        width: '100%',
        // alignItems: 'center',
        // backgroundColor: '#F1F1F7',
    },
    input: {
        width: '100%',
        fontSize: 14,
        borderBottomWidth: 1,
        borderBottomColor: '#333',
        textAlign: 'left',
        color: 'black',
        marginTop: Platform.OS === 'ios' ? 15 : 15,
    },
    inputPrice: {
        width: '100%',
        fontSize: 14,
        borderBottomWidth: 1,
        borderBottomColor: '#333',
        textAlign: 'left',
        color: 'black'
    }
});

//make this component available to the app
export default RegisterProduct;
