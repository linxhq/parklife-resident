import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Text, ScrollView, Image, SafeAreaView, Dimensions, TextInput, Button } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImageSlider from 'react-native-image-slider';
import Modal, { ModalContent } from 'react-native-modals';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { CheckBox } from 'react-native-elements'


export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

import AppIntroSlider from 'react-native-app-intro-slider';

interface State {
    showRealApp: boolean;
    visible: boolean;
    checkedCustomer: boolean;
    checkedResident: boolean;
}
//   image: {
    //     uri:
    //       'https://dexter.lib.mi.us/wp-content/uploads/2018/02/Hello-Summer.jpg',
    //   },
const slides = [
    {
      key: 'somethun',
      title: 'Title 1',
      text: 'Description.\nSay something cool',
      image: require('../../../assets/images/home/slide-1.jpg'),
      backgroundColor: '#59b2ab',
    },
    {
      key: 'somethun-dos',
      title: 'Title 2',
      text: 'Other cool stuff',
      image: require('../../../assets/images/home/slide-2.jpg'),
      backgroundColor: '#febe29',
    },
    {
      key: 'somethun1',
      title: 'Rocket guy',
      text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
      image: require('../../../assets/images/home/slide-3.jpg'),
      backgroundColor: '#22bcb5',
    }
];

interface Props extends ComponentProps {
    
}

class Home extends React.Component<Props, State> {
    _menu = null;
    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        
        return {
            // title: navigation.state.params.title,
            headerRight: <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Menu
                                ref={params.setMenuRef}
                                button={<Text style={{ marginRight: 10 }} onPress={params.showMenu}><Ionicons name="md-menu" size={30} color="#177EFB" style={{ marginRight: 15 }} /></Text>}
                            >   
                                <MenuItem onPress={params.redirectHome}>Trang chủ</MenuItem>
                                <MenuDivider />
                                <MenuItem onPress={params.redirectService}>Dịch vụ</MenuItem>
                                <MenuDivider />
                                <MenuItem onPress={params.redirectEvaluate}>Đánh giá</MenuItem>
                                <MenuDivider />
                                <MenuItem onPress={params.redirectNotice}>Thông báo</MenuItem>
                                <MenuDivider />
                                {/* <MenuItem onPress={params.redirectHome}>Đăng bài</MenuItem> */}
                                <MenuItem>Đăng bài</MenuItem>
                                <MenuDivider />
                                <MenuItem onPress={params.redirectLogin}>Thoát</MenuItem>
                            </Menu>
                        </View>,

        };
    };

    constructor(props) {
        super(props);
        this.state = {
            showRealApp: false,
            visible: false,
            checkedCustomer: false,
            checkedResident: false,
        };
    }

    componentDidMount() {
        this.setState({ visible: true });
        this.props.navigation.setParams({
            setMenuRef: this.setMenuRef
        });
        this.props.navigation.setParams({
            showMenu: this.showMenu
        });
        this.props.navigation.setParams({
            redirectHome: this.redirectHome
        });
        this.props.navigation.setParams({
            redirectService: this.redirectService
        });
        this.props.navigation.setParams({
            redirectEvaluate: this.redirectEvaluate
        });
        this.props.navigation.setParams({
            redirectNotice: this.redirectNotice
        });
        this.props.navigation.setParams({
            redirectLogin: this.redirectLogin
        });
    }

    isCustomer = () => {
        const { checkedCustomer, checkedResident } = this.state;
        if(checkedCustomer == true) {
            this.setState({ checkedCustomer: false });
        } else {
            this.setState({ checkedCustomer: true });
            this.setState({ checkedResident: false });
        }
    }

    isResident = () => {
        const { checkedCustomer, checkedResident } = this.state;
        if(checkedResident == true) {
            this.setState({ checkedResident: false });
        } else {
            this.setState({ checkedResident: true });
            this.setState({ checkedCustomer: false });
        }
    }

    setMenuRef = ref => {
        this._menu = ref;
    };

    redirectHome=()=>
    {
        this._menu.hide();
        this.props.navigation.navigate('Home');
    }
    redirectService=()=>
    {
        this._menu.hide();
        this.props.navigation.navigate('Service');
    }
    redirectEvaluate=()=>
    {
        this._menu.hide();
        this.props.navigation.navigate('Evaluate');
    }
    redirectNotice=()=>
    {
        this._menu.hide();
        this.props.navigation.navigate('Notice');
    }
    redirectLogin=()=>
    {
        this._menu.hide();
        this.props.navigation.navigate('Login');
    }
    
    showMenu = () => {
        this._menu.show();
    };

    _renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('DetailSlideHome')}
                style={{
                    flex: 1,
                    backgroundColor: item.backgroundColor,
                    alignItems: 'center',
                    justifyContent: 'space-around',
                    paddingBottom: 100,
                    height: normalizeH(180)
                }}
            >
                <Image style={styles.image} source={item.image} />
            </TouchableOpacity>
        );
    }

    _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        this.setState({ showRealApp: true });
    }

    _renderNextButton = () => {
        return (
          <View />
        );
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                        <View>
                            <Modal
                                visible={this.state.visible}
                                onTouchOutside={() => {
                                    this.setState({ visible: false });
                                }}
                            >
                                <ModalContent style={{ height: normalizeH(300), width: '80%', justifyContent: 'center', alignSelf: 'center' }}>
                                    <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center', width: normalizeW(300)}}>
                                        <Text style={{color: "#4695FC", fontSize: 15, fontWeight: 'bold'}}>XIN CHÀO</Text>
                                        <Text style={{fontSize: 13}}>Vui lòng xác nhân thông tin bên dưới</Text>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center'}}>
                                        <CheckBox
                                            center
                                            title='Khách'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            onPress={this.isCustomer}
                                            checked={this.state.checkedCustomer}
                                            containerStyle={{ width: normalizeW(300) }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center'}}>
                                        <CheckBox
                                            center
                                            title='Cư dân'
                                            checkedIcon='dot-circle-o'
                                            uncheckedIcon='circle-o'
                                            onPress={this.isResident}
                                            checked={this.state.checkedResident}
                                            containerStyle={{ width: normalizeW(300) }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center'}}>
                                        <TextInput
                                            style={{ 
                                                height: 40, 
                                                borderColor: 'gray', 
                                                borderWidth: 0.5, 
                                                width: normalizeW(300), 
                                                padding: 10,
                                                borderRadius: 5,
                                            }}
                                            keyboardType={'numeric'}
                                            placeholder="Vui lòng nhập mã cư dân"
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center', marginTop: 10}}>
                                        <Button
                                            title="Gửi"
                                            color="#4695FC"
                                            onPress={() => this.setState({ visible: false })}
                                        />
                                    </View>
                                </ModalContent>
                            </Modal>
                        </View>
                        <View style={{width: '100%', height: normalizeH(180)}}>
                            {/* <AppIntroSlider renderItem={this._renderItem} slides={slides} onDone={this._onDone}/> */}
                            <AppIntroSlider
                                slides={slides}
                                renderItem={this._renderItem}
                                showSkipButton={false}
                                renderNextButton={this._renderNextButton}
                                activeDotStyle={{backgroundColor: 'black'}}
                            />
                            {/* <ImageSlider images={[
                                'https://dexter.lib.mi.us/wp-content/uploads/2018/02/Hello-Summer.jpg',
                                'https://www.creativefabrica.com/wp-content/uploads/2018/04/Hello-Summer-1.jpg',
                                'https://www.creativefabrica.com/wp-content/uploads/2018/04/Hello-Summer-SVG-DXF-PNG-EPS-by-TheBlackCatPrints.jpg'
                            ]}/> */}
                        </View>
                        <View style={{width: '100%'}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Service')}>
                                <Text style={{ marginTop: 8, marginLeft: 5, fontSize: 13 }}>Chợ cư dân</Text>
                            </TouchableOpacity>
                            <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                    <View style={{ width: 200, height: 110, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '80%'}}>
                                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/slide-1.jpg')} />
                                            </View>
                                            <View style={{width: '100%', height: '20%', backgroundColor: 'pink'}}><Text style={{ padding: 5, fontSize: 11 }}>Phấn má thời trang</Text></View>
                                        </View>
                                    </View>
                                    <View style={{ width: 200, height: 110, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '80%'}}>
                                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/ca-chua.jpg')} />
                                            </View>
                                            <View style={{width: '100%', height: '20%', backgroundColor: '#73C7FD'}}><Text style={{ padding: 5, fontSize: 11 }}>Cà chua nhà trồng</Text></View>
                                        </View>
                                    </View>
                                    <View style={{ width: 200, height: 110, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '80%'}}>
                                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/buoi.jpg')} />
                                            </View>
                                            <View style={{width: '100%', height: '20%', backgroundColor: '#53E3B9'}}><Text style={{ padding: 5, fontSize: 11 }}>Bưởi quê siêu sạch</Text></View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                    <View style={{ width: 200, height: 110, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '80%'}}>
                                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                            </View>
                                            <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 13 }}>Sịt muỗi chính hãng</Text></View>
                                        </View>
                                    </View>
                                    <View style={{ width: 200, height: 110, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '80%'}}>
                                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/nuoc-rua-bon-cau.jpg')} />
                                            </View>
                                            <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 13 }}>Nước tẩy bồn cầu</Text></View>
                                        </View>
                                    </View>
                                    <View style={{ width: 200, height: 110, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '80%'}}>
                                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/ban-chai-danh-rang.jpg')} />
                                            </View>
                                            <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 13 }}>Bàn chải đánh răng cho bé</Text></View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('FlastlistResidentialMarket')}>
                                <View style={{ alignSelf: 'flex-end', marginTop: 5, marginRight: 5 }}><Text style={{ fontSize: 13 }}>Xem tất cả</Text></View>
                            </TouchableOpacity>
                        </View>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                    <View style={{ width: 300, height: 150, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/son-mong-tay.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>30000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/dem-lau-chan.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Đệm lau chân</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>20000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 300, height: 150, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/noi-com-dien.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Nồi cơm điện Hàn Quốc</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>1000000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/tu-lanh.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Tủ lạnh 2 cánh</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}> 4000000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 300, height: 150, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{width: '100%', height: 50}}>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                    </View>
                                                    <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                    <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                        <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                        <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                            {/* <View style={{width: '100%', height: normalizeH(180)}}>
                                <ImageSlider images={[
                                    'https://www.tiendauroi.com/uploads/default/original/2X/a/ad615d9f114897c1ec5013ff390be88be8f15e9e.jpeg',
                                    'https://storage.googleapis.com/senpoint-media-release/static/common/img/campaign_slide/dab829b0ab86373c5557aac9654de4e8.jpg',
                                    'https://www.creativefabrica.com/wp-content/uploads/2018/04/Hello-Summer-SVG-DXF-PNG-EPS-by-TheBlackCatPrints.jpg'
                                ]}/>
                            </View> */}
                            <View style={{width: '100%'}}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('FlastlistNotice')}>
                                    <Text style={{ marginTop: 8, marginLeft: 5, fontSize: 13 }}>Thông báo</Text>
                                </TouchableOpacity>
                                <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View>
                                <View style={{ marginTop: 10, width: '100%' }}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '80%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/login/background-login.jpg')} />
                                                </View>
                                                <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 8 }}>An Bình Plaza mở ...</Text></View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '80%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/toa-nha-2.jpg')} />
                                                </View>
                                                <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 8 }}>Vin Group mới ra ...</Text></View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '80%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/toa-nha-3.jpg')} />
                                                </View>
                                                <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 8 }}>Ap Building mới ra ...</Text></View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '80%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/login/background-login.jpg')} />
                                                </View>
                                                <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 8 }}>An Bình Plaza mở ...</Text></View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '80%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/toa-nha-2.jpg')} />
                                                </View>
                                                <View style={{width: '100%', height: '20%', backgroundColor: 'powderblue'}}><Text style={{ padding: 2, fontSize: 8 }}>Vin Group mới ra ...</Text></View>
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                            </View>

                            <View style={{width: '100%'}}>
                                <Text style={{ marginTop: 8, marginLeft: 5, fontSize: 13 }}>Dịch vụ gần đây</Text>
                                <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View>
                                <View style={{ marginTop: 10, width: '100%' }}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '100%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/laz.png')} />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '100%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/shopee.jpg')} />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '100%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/amazon.jpg')} />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '100%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/tiki.png')} />
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 85, height: 85, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: '100%'}}>
                                                    <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/udemy.jpg')} />
                                                </View>
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                            </View>
                            
                            <View style={{width: '100%'}}>
                                <View style={{ marginTop: 10, width: '100%' }}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                        <View style={{ width: 300, height: 150, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 300, height: 150, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{ width: 300, height: 150, marginRight: 10 }}>
                                            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <View style={{width: '100%', height: 50}}>
                                                    <View style={{flex: 1, flexDirection: 'row'}}>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <Image style={{width: '50%', height: '50%'}} source={require('../../../assets/images/home/xit-muoi.jpg')} />
                                                        </View>
                                                        <View style={{width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center'}}><Text style={{ fontSize: 13 }}>Thuốc sịt muỗi cao cấp</Text></View>
                                                        <View style={{width: '25%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                            <View style={{ borderWidth: 1, width: '80%', height: '30%', borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: normalizeH(300),
        resizeMode: 'cover'
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    title: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
    },
});

export default Home;