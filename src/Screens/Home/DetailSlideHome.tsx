//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, Dimensions, SafeAreaView } from 'react-native';
import IoniconsMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Fab, Icon, Button } from 'native-base';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

// create a component
class DetailSlideHome extends Component {
    constructor(props) {
        super(props)
        this.state = {
          active: true
        };
    }
    render() {
        return (
            // <View style={styles.container}>
            //     <Text style={styles.text}>DetailSlideHome</Text>
            // </View>
            <SafeAreaView style={{flex: 1}}>
                <ScrollView style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                    <View style={{width: '100%', height: normalizeH(180), backgroundColor: 'powderblue'}}>
                        <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/detail-slide-home.jpg')} />
                    </View>
                    <View style={{width: '100%', height: normalizeH(300), padding: 10}}>
                        <Text style={{ fontSize: 15 }}>2 THÁNG 9 NĂM 2019</Text>
                        <Text style={{ fontSize: 13, marginTop: 6 }}>Chào hè rực rỡ - FREESHIP mọi đơn hàng.</Text>
                        <Text style={{ fontSize: 13, marginTop: 6 }}>Chào ngày mới người bạn của Nhà. Mong hôm nay là một ngày tươi tắn và may mắn bạn nhé.</Text>
                        <Text style={{ fontSize: 13, marginTop: 6 }}>Bạn đã uống gì để sẵn sàng "bùng nổ" cùng mùa hè rực rỡ chưa nào ?</Text>
                        <Text style={{ fontSize: 13, marginTop: 6 }}>Trong hôm nay The Coffee Hourse sẽ không tính phí ship cho mọi đơn hàng khi đặt hàng giao tận nơi.</Text>
                        <Text style={{ fontSize: 13, marginTop: 6 }}>Thưởng thức món uống yêu thích, vừa đặt đã có, chẳng lo phí ship.</Text>
                        <Text style={{ fontSize: 13, marginTop: 6 }}>[*] Duy nhất ngày 02/09/2019.</Text>

                        <View style={{flexDirection: 'row'}}>
                            <IoniconsMaterialIcons name="contact-mail" size={25} color="#0B24FB" style={{ marginTop: 10, marginRight: 5 }} />
                            <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                <Text style={{ fontSize: 13, marginTop: 6}}>hi@thecoffeehourse.com</Text>
                            </View>
                        </View>
                    </View>
                    
                </ScrollView>
                <View>
                    <Fab
                        active={this.state.active}
                        direction="up"
                        containerStyle={{bottom: 80}}
                        style={styles.fab}
                        position="bottomRight"
                        onPress={() => this.setState({ active: !this.state.active })}
                    >
                        <Icon name="logo-whatsapp" />
                        {/* <Button style={{ backgroundColor: 'white' }}>
                            <Icon name="logo-facebook" />
                        </Button>
                        <Button style={{ backgroundColor: '#34AF23' }}>
                            <Icon name="logo-whatsapp" />
                        </Button> */}
                    </Fab>
                </View>
            </SafeAreaView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        // alignItems: 'center',
        // backgroundColor: '#2c3e50',
        padding: 10,
    },
    text: {
        color: 'black',
        fontSize: 13
    }
});

//make this component available to the app
export default DetailSlideHome;
