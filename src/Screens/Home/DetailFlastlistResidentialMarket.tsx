//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, TextInput } from 'react-native';
import IoniconsMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';


export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
  }
  
  export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
  }

// create a component
class DetailFlastlistResidentialMarket extends Component {
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{width: '100%', height: normalizeW(230), borderBottomWidth: 0.5}}>
                        <View style={styles.container}>
                            <View style={{ width: '50%', height: 200 }}>
                                <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/home/thung-rac.jpg')} />
                            </View>
                            <View style={{ width: '10%'}} />
                            <View style={{ width: '40%', height: 200, justifyContent: 'center' }}>
                                <View style={{ borderWidth: 1, borderColor: '#73C7FD', alignItems: 'center'}}><Text style={{ fontSize: 10, color: '#73C7FD' }}>100000 đ</Text></View>
                                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                    <IoniconsMaterialIcons name="contact-phone" size={25} color="#0B24FB" style={{ marginTop: 10, marginRight: 5 }} />
                                    <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                        <Text style={{ fontSize: 13, marginTop: 8, color: '#73C7FD'}}>0374033581</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{width: '100%',height: normalizeW(100), padding: 10, borderBottomWidth: 0.5}}>
                        <Text style={{ fontSize: 13, color: "#73C7FD" }}>Thông tin sản phẩm</Text>
                        <Text style={{ fontSize: 12, marginTop: 10 }}>
                            Thùng rác giá rẻ bất ngờ, gọi ngay để có được màu sắc mong muốn với giá khuyến mãi hấp dẫn.
                            Mình sẽ ưu đãi nếu các bạn gọi trong khung giờ vàng ( 10h - 12h sáng).
                        </Text>
                    </View>
                    <View style={{width: '100%', height: normalizeW(150), padding: 10}}>
                        <TouchableOpacity style={{ width: '100%', height: '10%', alignItems: 'flex-end' }}>
                            <Text style={{ fontSize: 13, color: "#73C7FD" }}>Xem tất cả</Text>
                        </TouchableOpacity>
                        <View style={{borderLeftWidth: 4, borderLeftColor: 'green',  width: '100%', height: '30%', borderWidth: 0.5, marginTop: 5 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '70%', padding: 5 }}>
                                    <Text style={{ fontSize: 12 }}>Người bán hàng rất nhiệt tình. Tốt</Text>
                                </View>
                                <View style={{ width: '30%', padding: 5 }}>
                                    <Text style={{ fontSize: 12 }}>30 tháng 6</Text>
                                    <Text style={{ fontSize: 12 }}>tuchido</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{borderLeftWidth: 4, borderLeftColor: '#BA2CD4', width: '100%', height: '30%', borderWidth: 0.5, marginTop: 5 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '70%', padding: 5 }}>
                                    <Text style={{ fontSize: 12 }}>Không nhiều màu để chọn lắm</Text>
                                </View>
                                <View style={{ width: '30%', padding: 5 }}>
                                    <Text style={{ fontSize: 12 }}>2 tháng 9</Text>
                                    <Text style={{ fontSize: 12 }}>tuchido</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{borderLeftWidth: 4, borderLeftColor: '#FC3173', width: '100%', height: '30%', borderWidth: 0.5, marginTop: 5 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '70%', padding: 5 }}>
                                    <Text style={{ fontSize: 12 }}>Địa chỉ hơi khó tìm</Text>
                                </View>
                                <View style={{ width: '30%', padding: 5 }}>
                                    <Text style={{ fontSize: 12 }}>24 tháng 10</Text>
                                    <Text style={{ fontSize: 12 }}>tuchido</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{width: '100%', height: normalizeW(180), padding: 10}}>
                        <View style={{ width: '100%', height: 120, padding: 5 }}>
                            <TextInput
                                multiline={true}
                                numberOfLines={4}
                                placeholder="Viết đánh giá của bạn ở đây"
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                keyboardType="email-address"
                                autoCapitalize="none"
                            />
                            <TouchableOpacity style={{ marginTop: 10, marginRight: 5, bottom: 0, right: 0, position: 'absolute'  }}>
                                <IoniconsFontAwesome name="send" size={15} color="black" />
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        padding: 10,
        flexDirection: 'row',
        width: '100%'
        // alignItems: 'center',
        // backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default DetailFlastlistResidentialMarket;
