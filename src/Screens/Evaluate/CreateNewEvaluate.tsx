//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import StarRating from 'react-native-star-rating';
import IoniconsMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';

var data = [{
    value: 'Dịch vụ sửa chữa nước',
}, {
    value: 'Dịch vụ sửa chữa nước',
}, {
    value: 'Dịch vụ chăm sóc sắc đẹp',
}];

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
  }
  
  export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
  }

// create a component
class CreateNewEvaluate extends Component {
    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        
        return {
            title: 'Đánh giá',
        };
    };
    constructor(props) {
        super(props);
        this.state = {
          starCount: 4
        };
    }
    onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Dropdown
                        label='Chọn lĩnh vực đánh giá'
                        data={data}
                    />
                </View>
                <View>
                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Tap to rate : </Text>
                </View>
                <View style={{ width: '50%', marginTop: 10 }}>
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={this.state.starCount}
                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                        starStyle={{color: '#53E3B9'}}
                        starSize={20}
                    />
                </View>
                <View style={{width: '100%', height: normalizeW(180)}}>
                    <View style={{ width: '100%', height: 120, padding: 5 }}>
                        <TextInput
                            multiline={true}
                            numberOfLines={4}
                            placeholder="Viết đánh giá của bạn ở đây"
                            underlineColorAndroid="transparent"
                            placeholderTextColor="black"
                            keyboardType="email-address"
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={{ padding: 10, justifyContent: 'flex-end', alignSelf: 'flex-end'}}>
                        <View style={{width: '100%', height: normalizeH(60)}}>
                            <TouchableOpacity>
                                <View style={{ marginTop: 5, marginRight: 5, backgroundColor: '#177EFB', padding: 10, borderRadius: 4 }}><Text style={{ fontSize: 13, marginLeft: 5, color: 'white' }}>Đánh giá</Text></View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        padding: 10,
        // flex: 1,
        justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default CreateNewEvaluate;
