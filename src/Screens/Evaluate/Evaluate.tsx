//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions, TextInput } from 'react-native';
import IoniconsMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import IoniconsMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Container, Header, Item, Input, Icon, Button } from 'native-base';
import { SearchBar } from 'react-native-elements';


export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
  }
  
  export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
  }

class LayoutSearchHeader extends React.Component {
    state = {
        search: '',
    };
    updateSearch = search => {
        this.setState({ search });
    };
    render() {
        const { search } = this.state;
        return (
            <SearchBar
                placeholder="Search..."
                onChangeText={this.updateSearch}
                value={search}
                containerStyle={{ width: '100%', backgroundColor: 'white', borderTopWidth: 0 }}
                inputContainerStyle={{ height: '100%' }}
                inputStyle={{ color: 'white', fontSize: 15 }}
                lightTheme={true}
                round={false}
            />
        );
    }
}

// create a component
class Evaluate extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: <LayoutSearchHeader />,
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column', backgroundColor: '#F1F1F7'}}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{ padding: 10 }}>
                        <View style={{width: '100%', height: normalizeH(70)}}>
                            <View style={{
                                borderLeftWidth: 4,  
                                width: '100%', height: '100%', 
                                borderLeftColor: '#28F362',
                                backgroundColor: 'white'
                            }}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '10%', height: '100%', alignItems: 'center'}}>
                                        <IoniconsMaterialCommunityIcons name="vote" size={30} color="#28F362" />
                                    </View>
                                    <View style={{width: '60%', height:'100%', flex: 1, justifyContent: 'center', alignSelf: 'center', marginLeft:3 }}>
                                        <Text style={{fontSize: 13, color: '#63BAFC'}}>Đánh giá nước sạch khu dân cư</Text>
                                    </View>
                                    <View style={{width: '30%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <View style={{ flexDirection: 'row'}}>
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                        </View>
                                    </View>
                                </View>
                                <View style={{flex: 1, flexDirection: 'row', borderTopWidth: 0.5, borderColor: 'pink'}}>
                                    <View style={{width: '10%', height: '100%' }}/>
                                    <View style={{width: '50%', height:'100%', flex: 1, justifyContent: 'center', alignSelf: 'center', marginLeft:3 }}>
                                        <Text style={{fontSize: 13}}>Mã số cư dân: M3001</Text>
                                    </View>
                                    <View style={{width: '40%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <Text style={{fontSize: 13}}>Ngày: 20/10/2019</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{ padding: 10 }}>
                        <View style={{width: '100%', height: normalizeH(130)}}>
                            <View style={{
                                borderLeftWidth: 4,  
                                width: '100%', height: '100%', 
                                borderLeftColor: '#FF6666',
                                backgroundColor: 'white'
                            }}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '10%', height: '100%', alignItems: 'center', marginTop: 3}}>
                                        <IoniconsFontAwesome name="warning" size={25} color="#FF6666" />
                                    </View>
                                    <View style={{width: '60%', height:'100%', flex: 1, justifyContent: 'center', alignSelf: 'center', marginLeft:3 }}>
                                        <Text style={{fontSize: 13, color: '#63BAFC'}}>Đánh giá an ninh khu dân cư</Text>
                                    </View>
                                    <View style={{width: '30%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <View style={{ flexDirection: 'row'}}>
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                        </View>
                                    </View>
                                </View>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '10%', height: '100%', alignItems: 'center', marginTop: 3}}>
                                        <View style={{ width: normalizeW(10), height: normalizeH(10), borderRadius: normalizeH(10)/2,  backgroundColor: 'red', top: 0, right: 0, position:'absolute' }}></View>
                                    </View>
                                    <View style={{width: '60%', height:'100%', flex: 1, marginLeft:3 }}>
                                        <Text style={{fontSize: 13}}>Hôm qua tôi đánh mất vé xe và thái độ của bảo vệ ...</Text>
                                    </View>
                                    <View style={{width: '30%', height: '100%', alignItems: 'center', justifyContent: 'center'}} />
                                </View>
                                <View style={{flex: 1, flexDirection: 'row', borderTopWidth: 0.5, borderColor: 'pink'}}>
                                    <View style={{width: '10%', height: '100%' }}/>
                                    <View style={{width: '50%', height:'100%', flex: 1, justifyContent: 'center', alignSelf: 'center', marginLeft:3 }}>
                                        <Text style={{fontSize: 13}}>Mã số cư dân: M3002</Text>
                                    </View>
                                    <View style={{width: '40%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <Text style={{fontSize: 13}}>Ngày: 10/03/2019</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{ padding: 10 }}>
                        <View style={{width: '100%', height: normalizeH(130)}}>
                            <View style={{
                                borderLeftWidth: 4,  
                                width: '100%', height: '100%', 
                                borderLeftColor: '#FDDC7A',
                                backgroundColor: 'white'
                            }}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '10%', height: '100%', alignItems: 'center', marginTop: 3}}>
                                        <IoniconsFontAwesome name="warning" size={25} color="#FDDC7A" />
                                    </View>
                                    <View style={{width: '60%', height:'100%', flex: 1, justifyContent: 'center', alignSelf: 'center', marginLeft:3 }}>
                                        <Text style={{fontSize: 13, color: '#63BAFC'}}>Đánh giá vệ sinh khu dân cư</Text>
                                    </View>
                                    <View style={{width: '30%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <View style={{ flexDirection: 'row'}}>
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star" size={15} color="#53E3B9" />
                                            <IoniconsFontAwesome name="star-half" size={15} color="#53E3B9" />
                                        </View>
                                    </View>
                                </View>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <View style={{width: '10%', height: '100%', alignItems: 'center', marginTop: 3}} />
                                    <View style={{width: '60%', height:'100%', flex: 1, marginLeft:3 }}>
                                        <Text style={{fontSize: 13}}>Gần khu nhà A3 có một số rác chưa được dọn dẹp vào ...</Text>
                                    </View>
                                    <View style={{width: '30%', height: '100%', alignItems: 'center', justifyContent: 'center'}} />
                                </View>
                                <View style={{flex: 1, flexDirection: 'row', borderTopWidth: 0.5, borderColor: 'pink'}}>
                                    <View style={{width: '10%', height: '100%' }}/>
                                    <View style={{width: '50%', height:'100%', flex: 1, justifyContent: 'center', alignSelf: 'center', marginLeft:3 }}>
                                        <Text style={{fontSize: 13}}>Mã số cư dân: M3003</Text>
                                    </View>
                                    <View style={{width: '40%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                        <Text style={{fontSize: 13}}>Ngày: 13/07/2019</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center'}}>
                        <View style={{width: '100%', height: normalizeH(60)}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateNewEvaluate')}>
                                <View style={{ marginTop: 5, marginRight: 5, backgroundColor: '#177EFB', padding: 5, borderRadius: 4 }}><Text style={{ fontSize: 13, marginLeft: 5, color: 'white' }}>Đánh giá mới</Text></View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        padding: 10,
        flexDirection: 'row',
        width: '100%'
        // alignItems: 'center',
        // backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Evaluate;
