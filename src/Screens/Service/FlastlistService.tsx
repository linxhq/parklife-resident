import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Text, ScrollView, Image, SafeAreaView, Dimensions, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Entypo';
import ImageSlider from 'react-native-image-slider';
import { Container, Header, Item, Input, Icon, Button } from 'native-base';
import { Searchbar } from 'react-native-paper';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import { SearchBar } from 'react-native-elements';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

interface State {
    showRealApp: boolean;
}

interface Props extends ComponentProps {
    
}

class LayoutSearchHeader extends React.Component {
    state = {
        search: '',
    };
    updateSearch = search => {
        this.setState({ search });
    };
    render() {
        const { search } = this.state;
        return (
            <SearchBar
                placeholder="Search..."
                onChangeText={this.updateSearch}
                value={search}
                containerStyle={{ width: '100%', backgroundColor: 'white', borderTopWidth: 0 }}
                inputContainerStyle={{ height: '100%' }}
                inputStyle={{ color: 'white', fontSize: 15 }}
                lightTheme={true}
                round={false}
            />
        );
    }
}

class FlastlistService extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: <LayoutSearchHeader />,
        // headerRight: null
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                {/* <View style={{ width: '100%', height: 150, marginRight: 10 }}> */}
                                    <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/son-mong-tay.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>4.5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 500 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/thung-rac.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/tham-lau-chan-in-hoa-tiet.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Đệm lau chân</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/son-mong-tay.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>4.5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 500 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/thung-rac.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/tham-lau-chan-in-hoa-tiet.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Đệm lau chân</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/son-mong-tay.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>4.5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 500 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/thung-rac.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/tham-lau-chan-in-hoa-tiet.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Đệm lau chân</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/son-mong-tay.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>4.5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 500 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/thung-rac.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/tham-lau-chan-in-hoa-tiet.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Đệm lau chân</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/son-mong-tay.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>4.5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 500 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/thung-rac.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Thùng rác thông minh</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={{width: '100%', height: 50, marginTop: 20}}>
                                            <View style={{flex: 1, flexDirection: 'row'}}>
                                                <View style={{width: '35%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                                    <Image style={{width: '75%', height: '100%'}} source={require('../../../assets/images/service/tham-lau-chan-in-hoa-tiet.jpg')} />
                                                </View>
                                                <View style={{width: '60%', height: '100%', justifyContent: 'center'}}>
                                                    <Text style={{ fontSize: 13 }}>Đệm lau chân</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ fontSize: 10 }}>5 </Text>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <Text style={{ fontSize: 10 }}> 323 lượt sử dụng</Text>
                                                    </View>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailService')}>
                                                        <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'flex-end' }}>
                                                            <Text style={{ fontSize: 10, color: '#73C7FD' }}>See more</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                {/* </View> */}
                                
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: normalizeH(300),
        resizeMode: 'cover'
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    title: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
    },
});

export default FlastlistService;