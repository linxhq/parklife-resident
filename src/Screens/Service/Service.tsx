import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Text, ScrollView, Image, SafeAreaView, Dimensions } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImageSlider from 'react-native-image-slider';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

import AppIntroSlider from 'react-native-app-intro-slider';

interface State {
    showRealApp: boolean;
}

interface Props extends ComponentProps {
    
}

class Service extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => ({
        // headerRight: <TouchableOpacity><Ionicons name="md-menu" size={30} color="#177EFB" style={{ marginRight: 15 }} /></TouchableOpacity>,
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });

    constructor(props) {
        super(props);
        this.state = {
            showRealApp: false,
        };
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                        <View style={{width: '100%'}}>
                            {/* <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View> */}
                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <View style={{ width: '40%' }}>
                                    <TouchableOpacity style={{alignSelf: 'flex-start'}}>
                                        <View style={{ marginTop: 5, marginRight: 5, backgroundColor: '#177EFB', padding: 5, borderRadius: 4 }}><Text style={{ fontSize: 13, marginLeft: 5, color: 'white' }}>Dịch vụ sửa chữa</Text></View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '60%', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{alignSelf: 'flex-end'}} onPress={() => this.props.navigation.navigate('FlastlistService')}>
                                        <View style={{ }}><Text style={{ fontSize: 13 }}>Xem tất cả</Text></View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/sua-chua-dieu-hoa-tai-nha-dai-tu.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ sửa chữa điều hòa ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>200 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/sua-tu-lanh-1.png')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ sửa chữa tủ lạnh ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>300 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/dich-vu-ve-sinh-van-phong.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ dọn nhà ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>150 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/suachuamang.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ Internet ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>200 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                        <View style={{width: '100%'}}>
                            {/* <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View> */}
                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <View style={{ width: '40%' }}>
                                    <TouchableOpacity style={{alignSelf: 'flex-start'}}>
                                        <View style={{ marginTop: 5, marginRight: 5, backgroundColor: '#177EFB', padding: 5, borderRadius: 4 }}><Text style={{ fontSize: 13, marginLeft: 5, color: 'white' }}>Dịch vụ giặt là</Text></View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '60%', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{alignSelf: 'flex-end'}} onPress={() => this.props.navigation.navigate('FlastlistService')}>
                                        <View style={{ }}><Text style={{ fontSize: 13 }}>Xem tất cả</Text></View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/dich-vu-ui-quan-ao-685.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Hệ thống giặt là ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>333 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/thiet-bi-giat-la.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ giặt là công nghiệp ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>545 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/dich-vu-giat-la-long-bien.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ giặt là tốt ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>123 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/1.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ giặt là giá rẻ ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>323 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>

                    <View style={{flex: 1, flexDirection: 'column', height: '100%'}}>
                        <View style={{width: '100%'}}>
                            {/* <View style={{ marginTop: 10, width: '100%', borderBottomWidth: 0.5, borderBottomColor: 'pink' }}></View> */}
                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <View style={{ width: '40%' }}>
                                    <TouchableOpacity style={{alignSelf: 'flex-start'}}>
                                        <View style={{ marginTop: 5, marginRight: 5, backgroundColor: '#177EFB', padding: 5, borderRadius: 4 }}><Text style={{ fontSize: 13, marginLeft: 5, color: 'white' }}>Dịch vụ ăn uống</Text></View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: '60%', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{alignSelf: 'flex-end'}} onPress={() => this.props.navigation.navigate('FlastlistService')}>
                                        <View style={{ }}><Text style={{ fontSize: 13 }}>Xem tất cả</Text></View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{width: '100%'}}>
                            <View style={{ marginTop: 10, width: '100%' }}>
                                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{ marginLeft: 10 }}>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/image001.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Tổ chức sự kiện ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>2322 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/tien-tra-1.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Dịch vụ tiệc buffet, tiệc trà ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>4341 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/1532580573004_7526583.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Ăn buffet sang chảnh ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>12343 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ width: 120, height: 170, marginRight: 10 }}>
                                        <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                                            <View style={{width: '100%', height: '65%', borderTopLeftRadius: 4, borderTopRightRadius: 4, overflow: "hidden"}}>
                                                <Image style={{borderTopLeftRadius: 4, borderTopRightRadius: 4, width: '100%', height: '100%'}} source={require('../../../assets/images/service/120863-body1.jpg')} />
                                            </View>
                                            <View style={{flexDirection: 'row', borderBottomLeftRadius: 4, borderBottomRightRadius: 4, width: '100%', height: '25%', backgroundColor: 'white'}}>
                                                <View style={{ width: '40%', marginTop: 4, marginLeft: 4 }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>Buffet trưa/tối đẳng cấp ...</Text>
                                                </View>
                                                <View style={{ width: '60%', marginTop: 4, marginLeft: 4, alignItems: 'center' }}>
                                                    <Text style={{ padding: 2, fontSize: 8 }}>4.5</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                                        <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                                    </View>
                                                    <Text style={{ padding: 2, fontSize: 6 }}>432 lượt sử dụng</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E9ECF3'
    },
    image: {
        width: '100%',
        height: normalizeH(300),
        resizeMode: 'cover'
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    title: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
    },
});

export default Service;