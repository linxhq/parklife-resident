//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Platform } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Dropdown } from 'react-native-material-dropdown';

var data = [{
    value: 'contact@linxhq.com',
}, {
    value: 'tula@linxhq.com',
}, {
    value: 'haint@linxhq.com',
}];

// create a component
class SendEmail extends Component {
    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        
        return {
            // title: navigation.state.params.title,
            headerRight: <Ionicons name="md-send" size={30} color="black" style={{ marginRight: 15 }} />,

        };
    };
    render() {
        return (
            <View style={styles.container}>
                {/* <View style={{ flexDirection: 'row' }}> */}
                    <View style={{ width: '100%' }}>
                        {/* <View> */}
                            <Dropdown
                                label='To'
                                data={data}
                            />
                        {/* </View> */}
                    </View>
                    <View style={{ width: '100%', marginTop: 10 }}>
                        <TextInput
                            placeholder="From"
                            underlineColorAndroid="transparent"
                            placeholderTextColor="black"
                            style={styles.input}
                        />
                    </View>
                    <View style={{ width: '100%', marginTop: 10 }}>
                        <TextInput
                            placeholder="Subject"
                            underlineColorAndroid="transparent"
                            placeholderTextColor="black"
                            style={styles.input}
                        />
                    </View>
                    <View style={{ width: '100%', marginTop: 10 }}>
                        <TextInput
                            placeholder="Compose email"
                            underlineColorAndroid="transparent"
                            placeholderTextColor="black"
                            style={styles.input}
                        />
                    </View>
                {/* </View> */}
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: '#2c3e50',
    },
    input: {
        width: '100%',
        fontSize: 14,
        borderBottomWidth: 0.5,
        borderBottomColor: '#333',
        textAlign: 'left',
        color: 'black',
        marginTop: Platform.OS === 'ios' ? 15 : 15,
    },
});

//make this component available to the app
export default SendEmail;
