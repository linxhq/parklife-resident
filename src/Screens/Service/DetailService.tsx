//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, Dimensions, SafeAreaView, TouchableOpacity, TouchableHighlight } from 'react-native';
import IoniconsMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Fab, Icon, Button } from 'native-base';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import IoniconsEntypo from 'react-native-vector-icons/Entypo';
import IoniconsFontisto from 'react-native-vector-icons/Fontisto';
import IoniconsIonicons from 'react-native-vector-icons/Ionicons';
import IoniconsFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Dialog from "react-native-dialog";
import Modal, { ModalContent } from 'react-native-modals';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

// create a component
class DetailService extends Component {
    constructor(props) {
        super(props)
        this.state = {
          active: true,
          dialogVisiblePhone: false,
          dialogVisibleSocial: false
        };
    }

    showDialogPhone = () => {
        this.setState({ dialogVisiblePhone: true });
    };
    
    handleCancelPhone = () => {
        this.setState({ dialogVisiblePhone: false });
    };
    
    handleDeletePhone = () => {
        // The user has pressed the "Delete" button, so here you can do your own logic.
        // ...Your logic
        this.setState({ dialogVisiblePhone: false });
    };

    showDialogSocial = () => {
        this.setState({ dialogVisibleSocial: true });
    };

    render() {
        return (
            // <View style={styles.container}>
            //     <Text style={styles.text}>DetailSlideHome</Text>
            // </View>
                <SafeAreaView style={{flex: 1}}>
                    <ScrollView style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                        <View style={{width: '100%', height: normalizeH(200), backgroundColor: 'powderblue'}}>
                            <Image style={{width: '100%', height: '100%'}} source={require('../../../assets/images/service/son-mong-tay.jpg')} />
                        </View>
                        <View style={{width: '100%', height: normalizeH(300), padding: 10}}>
                            <Text style={{ fontSize: 13 }}>Sơn móng tay đẹp</Text>
                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                <Text style={{ fontSize: 10 }}>4.5 </Text>
                                <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                <IoniconsFontAwesome name="star" size={10} color="#53E3B9" />
                                <IoniconsFontAwesome name="star-half" size={10} color="#53E3B9" />
                                <Text style={{ fontSize: 10 }}> 500 lượt sử dụng</Text>
                            </View>
                            <Text style={{ fontSize: 15, marginTop: 5 }}>Để xuất hiện với vẻ ngoài trẻ trung và rạng rỡ nhất, phái nữ luôn phải mang theo bên mình những phụ kiện trang điểm như son phấn, kẻ mắt, mascara,… Trong đó không thể không kể đến một phụ kiện vô cùng quan trọng đó chính là sơn móng tay</Text>

                            <View style={{flexDirection: 'row'}}>
                                <IoniconsEntypo name="location" size={25} color="#F49234" style={{ marginTop: 10, marginRight: 5 }} />
                                <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                    <Text style={{ fontSize: 13, marginTop: 6}}>Số 87, Trần thái tông, Cầu giấy, Hà Nội</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <IoniconsEntypo name="phone" size={25} color="#F49234" style={{ marginTop: 10, marginRight: 5 }} />
                                <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                    <Text style={{ fontSize: 13, marginTop: 6}}>02455452323</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={{flexDirection: 'row'}} onPress={() => this.props.navigation.navigate('SendEmail')}>
                                {/* <View style={{flexDirection: 'row'}}> */}
                                    <IoniconsFontisto name="email" size={25} color="#F49234" style={{ marginTop: 10, marginRight: 5 }} />
                                    <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                        <Text style={{ fontSize: 13, marginTop: 6}}>contact@linxhq.com</Text>
                                    </View>
                                {/* </View> */}
                            </TouchableOpacity>
                            <View style={{flexDirection: 'row'}}>
                                <IoniconsFontAwesome name="wpforms" size={25} color="#F49234" style={{ marginTop: 10, marginRight: 5 }} />
                                <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                    <Text style={{ fontSize: 13, marginTop: 6}}>E-form</Text>
                                </View>
                            </View>
                        </View>
                        
                    </ScrollView>
                    <View>
                        <Fab
                            active={this.state.active}
                            direction="up"
                            containerStyle={{bottom: 80}}
                            style={styles.fab}
                            position="bottomRight"
                            // onPress={() => this.setState({ active: !this.state.active })}
                        >
                            <IoniconsEntypo name="chat" onPress={this.showDialogSocial}/>
                            {/* <Button style={{ backgroundColor: '#34AF23' }}>
                                <Icon name="logo-facebook" />
                            </Button> */}
                            <Button style={{ backgroundColor: '#34AF23' }} onPress={this.showDialogPhone}>
                                <Icon name="logo-whatsapp" />
                            </Button>
                        </Fab>
                    </View>
                    <View>
                        <Dialog.Container visible={this.state.dialogVisiblePhone}>
                            <Dialog.Title>02455452323</Dialog.Title>
                            {/* <Dialog.Description>
                                Do you want to delete this account? You cannot undo this action.
                            </Dialog.Description> */}
                            <Dialog.Button label="Hủy" onPress={this.handleCancelPhone} />
                            <Dialog.Button label="Gọi" onPress={this.handleDeletePhone} />
                        </Dialog.Container>
                        <Modal
                            visible={this.state.dialogVisibleSocial}
                            // swipeDirection={['up', 'down']} // can be string or an array
                            // swipeThreshold={200} // default 100
                            // onSwipeOut={(event) => {
                            //     this.setState({ visible: false });
                            // }}
                            onTouchOutside={() => {
                                this.setState({ dialogVisibleSocial: false });
                            }}
                        >
                            <ModalContent style={{ height: normalizeH(140), width: '80%', justifyContent: 'center', alignSelf: 'center' }}>
                                <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
                                    <Text>Vui lòng chọn ứng dụng Chat</Text>
                                </View>
                                <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
                                    <TouchableOpacity onPress={()=>{}} style={{width: '33.3%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                        <IoniconsIonicons name="logo-whatsapp" onPress={this.showDialogSocial} size={50} color="#28F62D"/>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{width: '33.3%', height: 50, justifyContent: 'center', alignItems: 'center'}}>
                                        <IoniconsFontAwesome5 name="facebook-messenger" onPress={this.showDialogSocial} size={50} color="#00B7FF"/>    
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{width: '33.3%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                        <IoniconsFontAwesome5 name="viber" onPress={this.showDialogSocial} size={50} color="#00B7FF"/>
                                    </TouchableOpacity>
                                </View>
                            </ModalContent>
                        </Modal>
                    </View>
            </SafeAreaView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        // alignItems: 'center',
        // backgroundColor: '#2c3e50',
        padding: 10,
    },
    text: {
        color: 'black',
        fontSize: 13
    }
});

//make this component available to the app
export default DetailService;
