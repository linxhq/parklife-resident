import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Text, ScrollView, Image, SafeAreaView, Dimensions, TextInput, Button } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImageSlider from 'react-native-image-slider';
import Modal, { ModalContent } from 'react-native-modals';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { CheckBox } from 'react-native-elements';
import IoniconsAntDesign from 'react-native-vector-icons/AntDesign';


export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

import AppIntroSlider from 'react-native-app-intro-slider';

interface State {
    showRealApp: boolean;
    visible: boolean;
    checkedCustomer: boolean;
    checkedResident: boolean;
}

interface Props extends ComponentProps {
    
}

class Home extends React.Component<Props, State> {
    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        
        return {
            title: 'Thông báo'
        };
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', height: '100%', padding: 10}}>
                        <Text style={{ fontSize: 13 }}>Mới nhất</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailNotice')}>
                            <View style={{width: '100%', height: normalizeH(60), marginTop: 10, backgroundColor: "#F1F1F7"}}>
                                <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
                                    <View style={{width: '15%'}}>
                                        <Image style={{ width: '100%', height: '100%'}} source={require('../../../assets/images/notice/116691.jpg')} />
                                    </View>
                                    <View style={{width: '5%'}} />
                                    <View style={{width: '72%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <Text style={{ fontSize: 13 }}>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi. Xin vui lòng gửi đánh giá của bạn.</Text>
                                    </View>
                                    <View style={{width: '13%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <IoniconsAntDesign name="arrowright" size={20} color="black" />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <Text style={{ fontSize: 13, marginTop: 10 }}>Đã đọc</Text>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailNotice')}>
                            <View style={{width: '100%', height: normalizeH(60), marginTop: 10, backgroundColor: "#F1F1F7"}}>
                                <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
                                    <View style={{width: '15%'}}>
                                        <Image style={{ width: '100%', height: '100%'}} source={require('../../../assets/images/notice/kaitea.png')} />
                                    </View>
                                    <View style={{width: '5%'}} />
                                    <View style={{width: '72%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <Text style={{ fontSize: 13 }}>
                                            Được phục vụ bạn là niềm vui của chúng tôi.
                                            Xin cảm ơn
                                        </Text>
                                    </View>
                                    <View style={{width: '13%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <IoniconsAntDesign name="arrowright" size={20} color="black" />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailNotice')}>
                            <View style={{width: '100%', height: normalizeH(60), marginTop: 10, backgroundColor: "#F1F1F7"}}>
                                <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
                                    <View style={{width: '15%'}}>
                                        <Image style={{ width: '100%', height: '100%'}} source={require('../../../assets/images/notice/matcha.png')} />
                                    </View>
                                    <View style={{width: '5%'}} />
                                    <View style={{width: '72%', justifyContent: 'center', alignSelf: 'center'}}>
                                    <Text style={{ fontSize: 13 }}>
                                            Được phục vụ bạn là niềm vui của chúng tôi.
                                            Xin cảm ơn
                                        </Text>
                                    </View>
                                    <View style={{width: '13%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <IoniconsAntDesign name="arrowright" size={20} color="black" />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailNotice')}>
                            <View style={{width: '100%', height: normalizeH(60), marginTop: 10, backgroundColor: "#F1F1F7"}}>
                                <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
                                    <View style={{width: '15%'}}>
                                        <Image style={{ width: '100%', height: '100%'}} source={require('../../../assets/images/notice/highlands.jpg')} />
                                    </View>
                                    <View style={{width: '5%'}} />
                                    <View style={{width: '72%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <Text style={{ fontSize: 13 }}>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi. Xin vui lòng gửi đánh giá của bạn.</Text>
                                    </View>
                                    <View style={{width: '13%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <IoniconsAntDesign name="arrowright" size={20} color="black" />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailNotice')}>
                            <View style={{width: '100%', height: normalizeH(60), marginTop: 10, backgroundColor: "#F1F1F7"}}>
                                <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
                                    <View style={{width: '15%'}}>
                                        <Image style={{ width: '100%', height: '100%'}} source={require('../../../assets/images/notice/bbq.jpg')} />
                                    </View>
                                    <View style={{width: '5%'}} />
                                    <View style={{width: '72%', justifyContent: 'center', alignSelf: 'center'}}>
                                    <Text style={{ fontSize: 13 }}>
                                            Được phục vụ bạn là niềm vui của chúng tôi.
                                            Xin cảm ơn
                                        </Text>
                                    </View>
                                    <View style={{width: '13%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <IoniconsAntDesign name="arrowright" size={20} color="black" />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailNotice')}>
                            <View style={{width: '100%', height: normalizeH(60), marginTop: 10, backgroundColor: "#F1F1F7"}}>
                                <View style={{flex: 1, flexDirection: 'row', height: '100%'}}>
                                    <View style={{width: '15%'}}>
                                        <Image style={{ width: '100%', height: '100%'}} source={require('../../../assets/images/notice/goc-ha-noi.png')} />
                                    </View>
                                    <View style={{width: '5%'}} />
                                    <View style={{width: '72%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <Text style={{ fontSize: 13 }}>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi. Xin vui lòng gửi đánh giá của bạn.</Text>
                                    </View>
                                    <View style={{width: '13%', justifyContent: 'center', alignSelf: 'center'}}>
                                        <IoniconsAntDesign name="arrowright" size={20} color="black" />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

export default Home;