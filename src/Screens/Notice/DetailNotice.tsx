//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

// create a component
class DetailNotice extends Component {
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'column', width: '100%'}}>
                <View style={{width: '100%', height: '10%', backgroundColor: '#51A4FC', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>Cảm ơn Quý khách!</Text>
                </View>
                <View style={{width: '100%', height: '80%', backgroundColor: 'white', padding: 10}}>
                    <View style={{ flexDirection: 'row'}}>
                        <View style={{ width: '20%' }}></View>
                        <View style={{ width: '80%' }}>
                            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 14}}>Super Admin</Text>
                            <Text style={{color: '#9EA2A9', fontWeight: 'bold', fontSize: 11}}>Mon, Oct 7 2019, 9:43 AM</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={{ fontSize: 13, marginTop: 10 }}>Super trân trọng cảm ơn quý khách đã tin tưởng và lựa chọn dịch vụ sửa chữa của Super.</Text>
                        <Text style={{ fontSize: 13, marginTop: 10 }}>Để Super có thể nâng cao chất lượng dịch vụ. Quý khách vui lòng giúp chúng tôi đánh giá mức độ hài lòng về trải nghiệm của bạn khi sử dụng dịch vụ của Supper.</Text>
                        <Text style={{ fontSize: 13, marginTop: 10 }}>Mọi cảm nhận và ý kiến đóng góp của quý khách sẽ là nền tảng vững chắc cho sự phát triển của chúng tôi.</Text>
                    </View>
                    <View style={{ padding: 10, justifyContent: 'center', alignSelf: 'center'}}>
                        <View style={{width: '100%', height: 60}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('CreateEvaluateNotice')}>
                                <View style={{ marginTop: 5, marginRight: 5, backgroundColor: '#177EFB', padding: 10, borderRadius: 4 }}><Text style={{ fontSize: 13, marginLeft: 5, color: 'white' }}>Đánh giá</Text></View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{width: '100%', height: '10%', backgroundColor: '#51A4FC', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>87 Trần thái tông, Cầu giấy, Hà Nội</Text>
                    <Text style={{ fontSize: 16, color: 'white', fontWeight: 'bold' }}>Tầng 6 AP Building</Text>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default DetailNotice;
