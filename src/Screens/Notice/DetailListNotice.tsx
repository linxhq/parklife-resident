import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, ScrollView, SafeAreaView, Dimensions, ImageBackground } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, Text, Image } from 'native-base';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

interface State {
    showRealApp: boolean;
}

interface Props extends ComponentProps {
    
}

class DetailListNotice extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => ({
        headerRight: <TouchableOpacity><Ionicons name="md-menu" size={30} color="#177EFB" style={{ marginRight: 15 }} /></TouchableOpacity>,
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });

    render() {
        const { width, height } = Dimensions.get('screen');
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', padding: 10}}>
                        <View style={{width: '100%', height: normalizeH(300)}}>
                            <ImageBackground
                                style={{ width: '100%', height: normalizeH(300) }}
                                resizeMode="cover"
                                source={require('../../../assets/images/login/background-login.jpg')}
                            >
                                <View style={{ width: '100%', height: 50, position: 'absolute', bottom: 0, backgroundColor: '#87BAE6', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 13 }}>An Bình Plaza bán căn hộ cao cấp ...</Text>
                                </View>
                            </ImageBackground>
                        </View>
                        <View style={{width: '100%', height: normalizeH(400), marginTop: 10}}>
                            <Text style={{ fontSize: 13 }}>
                                An Bình Plaza là dự án chung cư căn hộ cao cấp của chủ đầu tư Tập đoàn Geleximco với nhiều tiện ích cao cấp phù hợp dành cho cư dân, với vị trí đắc địa trung tâm Mỹ Đình – nơi giao thoa giữa quận Cầu Giấy và Nam Từ Liêm, Hà Nội 
                            </Text>
                            <Text style={{ fontSize: 13, marginTop: 10 }}>
                                Tọa lạc tại cửa ngõ phía tây Thủ đô, là nơi giao thoa của 03 tuyến đường huyết mạch Hồ Tùng Mậu – Xuân Thủy; Phạm Hùng – Nội Bài và Tuyến số 3 Đường sắt đô thị Nhổn – Ga Hà Nội, gần như mọi ngả đường đều dẫn đến An Bình Plaza.
                            </Text>
                            <Text style={{ fontSize: 13, marginTop: 10 }}>
                                An Bình Plaza có địa chỉ tại 2 mặt đường: 97 Trần Bình và 16 Phạm Hùng, phường Mỹ Đình, một vị trí đắc địa của quận Nam Từ Liêm, dễ dàng di chuyển đến các trường đại học lớn như Học viện Báo chí và Tuyên truyền, Đại học Thương mại, Đại học Quốc gia, Đại học Sư phạm,… trở thành trung tâm cho những tân sinh viên mới. Hơn nữa từ chung cư 97 Trần Bình, bạn còn dễ dàng di chuyển đến trung tâm mua sắm BigC, IPH, The Garden, Trung tâm hội nghị quốc gia, bến xe Mỹ Đình, …. vô cùng thuận tiện và dễ dàng.
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default DetailListNotice;