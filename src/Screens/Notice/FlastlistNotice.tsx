import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, ScrollView, SafeAreaView, Dimensions, ImageBackground } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, Text, Image } from 'native-base';

export function normalizeW(val) {
    // normalize based on width
    const ratio = val / 375; // 375 is based width of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').width);
    return newVal;
}
  
export function normalizeH(val) {
    // normalize based on height
    const ratio = val / 736; // 736 is based height of iphone 7
    const newVal = Math.round(ratio * Dimensions.get('window').height);
    return newVal;
}

interface State {
    showRealApp: boolean;
}

interface Props extends ComponentProps {
    
}

class FlastlistNotice extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => ({
        headerRight: <TouchableOpacity><Ionicons name="md-menu" size={30} color="#177EFB" style={{ marginRight: 15 }} /></TouchableOpacity>,
        // headerLeft: <TouchableOpacity><Image style={{width:20, height:20, marginLeft: 15}} source={require('../../images/icons/search.png')} /></TouchableOpacity>,
    });

    render() {
        const { width, height } = Dimensions.get('screen');
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{flex: 1, flexDirection: 'column', padding: 10}}>
                        <View style={{width: '100%', height: normalizeH(300)}}>
                            <ImageBackground
                                style={{ width: '100%', height: normalizeH(300) }}
                                resizeMode="cover"
                                source={require('../../../assets/images/login/background-login.jpg')}
                            >
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailListNotice')}>
                                    <Text style={{ color: '#F49234', fontSize: 13, position:'absolute', top: 5, right: 5 }}>See more</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, position: 'absolute', bottom: 0, backgroundColor: '#87BAE6', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 13 }}>An Bình Plaza bán căn hộ cao cấp ...</Text>
                                </View>
                            </ImageBackground>
                        </View>
                        <View style={{width: '100%', height: normalizeH(300), marginTop: 10}}>
                            <ImageBackground
                                style={{ width: '100%', height: normalizeH(300) }}
                                resizeMode="cover"
                                source={require('../../../assets/images/home/toa-nha-2.jpg')}
                            >
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailListNotice')}>
                                    <Text style={{ color: '#F49234', fontSize: 13, position:'absolute', top: 5, right: 5 }}>See more</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, position: 'absolute', bottom: 0, backgroundColor: '#87BAE6', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 13 }}>Vin Group mới ra mắt resort mới tiêu chuẩn 5 sao ...</Text>
                                </View>
                            </ImageBackground>
                        </View>
                        <View style={{width: '100%', height: normalizeH(300), marginTop: 10}}>
                            <ImageBackground
                                style={{ width: '100%', height: normalizeH(300) }}
                                resizeMode="cover"
                                source={require('../../../assets/images/home/toa-nha-3.jpg')}
                            >
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailListNotice')}>
                                    <Text style={{ color: '#F49234', fontSize: 13, position:'absolute', top: 5, right: 5 }}>See more</Text>
                                </TouchableOpacity>
                                <View style={{ width: '100%', height: 50, position: 'absolute', bottom: 0, backgroundColor: '#87BAE6', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 13 }}>Ap Building mới ra mắt resort mới tiêu chuẩn 5 sao ...</Text>
                                </View>
                            </ImageBackground>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default FlastlistNotice;