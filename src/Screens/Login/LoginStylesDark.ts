import { StyleSheet, Platform } from 'react-native';
import { ColorApp, BackgroundApp, FontApp, FontSizeTextApp } from '../../Utils/Styles/styles';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'rgba(0, 0, 0, 0.5)'
      },
      logo: {
        width: 300,
        height: Platform.OS === 'ios' ? 150 : 140,
        resizeMode: 'contain',
        marginTop: Platform.OS === 'ios' ? 60 : 40
      },
      form: {
        width: '70%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: Platform.OS === 'ios' ? 20 : 10
        // height: '70%'
      },
      formWrapper: {
        // marginTop: 20,
        flexDirection: 'column',
        // justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%'
      },
      input: {
        width: '75%',
        fontSize: 14,
        margin: 13,
        borderBottomWidth: 1,
        borderBottomColor: 'white',
        textAlign: 'left',
        color: 'white',
        padding: 3,
        marginTop: Platform.OS === 'ios' ? 20 : 15,
        marginBottom: Platform.OS === 'ios' ? 20 : 15
      },
      rememberMe: {
        color: '#EFEFEF',
        fontSize: 11,
        fontWeight: 'bold'
      },
      checkboxContainer: {
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
      },
      socialHeading: {
        marginTop: 40,
        color: 'white',
        fontSize: 11,
        fontWeight: 'bold'
      },
      socialContainer: {
        marginTop: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },
      social: {
        width: 30,
        height: 30,
        resizeMode: 'contain',
        marginHorizontal: 20
      },
      loading: {
        marginVertical: 140
      },
      textNewAccount: {
        marginTop: 40,
        color: 'white',
        fontSize: 12,
        fontWeight: 'normal'
      },
});