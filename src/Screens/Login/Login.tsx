// Import Libary - React native
import React, { Component } from 'react'
import {
    ImageBackground,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    KeyboardAvoidingView,
    Dimensions,
  } from 'react-native';

import { GradientButton } from '../../Components';

// Import Libary 3rd
import LinearGradient from 'react-native-linear-gradient';

// Styles Application
import HeaderStyles from "../../Utils/HeaderStyles/HeaderStyles";
import { ColorApp, BackgroundApp, FontApp, FontSizeTextApp } from '../../Utils/Styles/styles';
import styles from './LoginStyles';
import stylesDark from './LoginStylesDark';

import { RootState, UserState } from '../../Reducers';
import { NavigationOptions, ComponentProps } from '../../Configs/ Interfaces';
import {showInfoMessage, showErrorMessage} from '../../Utils/showErrors';
import authService from '../../Services/AuthService';
import { Switch } from 'react-native-switch';
import SwitchSelector from "react-native-switch-selector";

interface Props extends ComponentProps {
    updateUser(data);
    loadUser();
    user: UserState;
}

interface State {
    username: string;
    password: string;
    isloading: boolean;
    switch: boolean;
}

const options = [
    { label: "01:00", value: "1" },
    { label: "01:30", value: "1.5" },
    { label: "02:00", value: "2" }
];
  

class Login extends React.Component<Props, State> {
    // static navigationOptions = {
    //     title: 'Login',
    //     ...HeaderStyles,
    // };

    constructor(props) {
        super(props);
        this.state = {
          username: '',
          password: '',
          isloading: false,
          switch: false,
        };
    }

    logIn() {
        const { username, password } = this.state;
        // if (!username || !password) {
        //     return showInfoMessage('Please fill in all fields');
        // }

        this.setState({ isloading: true });

        // setTimeout(() => {
            this.props.navigation.navigate('Home');
        // }, 2000);
        

        // authService.signInWithEmailPassword({ email, password }).catch(err => {
        // console.log('err', err);
        // this.setState({ isloading: false });

        // if (err.message) {
        //     showErrorMessage(err.message);
        // }
        // });
    }

    _changeSwitch(val) {
        // alert(val);
        if(val == 'd') {
            // redirect screen dark
            // this.props.navigation.navigate('Login');
            this.setState({switch: true});
        }
        if (val == 'l') {
            // redirect screen light
            // this.props.navigation.navigate('LoginDark');
            this.setState({switch: false});
        }
    }

    render() {
        const { username, password } = this.state;
        const { width, height } = Dimensions.get('screen');
        return (
            <View>
                {this.state.switch ? (
                    <View>
                        {/* dark theme */}
                        <KeyboardAvoidingView behavior="padding">
                        <View>
                            <ImageBackground
                                style={{ width, height }}
                                resizeMode="cover"
                                source={require('../../../assets/images/login/background-login-dark.jpg')}
                            >
                            <View style={stylesDark.container}>
                                {/* <Image style={styles.logo} source={require('../../../assets/images/login/background-login.jpg')} /> */}

                                {/* {this.state.isloading ? (
                                <View>
                                    <ActivityIndicator size="large" style={styles.loading} color="black"/>
                                </View>
                                ) : ( */}
                                <View style={stylesDark.form}>
                                    <View style={stylesDark.formWrapper}>
                                    <TextInput
                                        placeholder="Username"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="white"
                                        style={stylesDark.input}
                                        value={username}
                                        keyboardType="email-address"
                                        autoCapitalize="none"
                                        onChangeText={val => this.setState({ username: val })}
                                    />
                                    <TextInput
                                        placeholder="Password"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor="white"
                                        style={stylesDark.input}
                                        value={password}
                                        secureTextEntry={true}
                                        onChangeText={val => this.setState({ password: val })}
                                    />
                                    </View>

                                    <GradientButton
                                    containerStyle={{ marginTop: 40 }}
                                    textStyle={{ backgroundColor: 'black', color: 'white', fontSize: 15 }}
                                    text="LOGIN"
                                    onPress={() => this.logIn()}
                                    />

                                    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                    <Text style={stylesDark.socialHeading}>OR SIGN IN WITH:</Text>

                                    <View style={stylesDark.socialContainer}>
                                        <TouchableOpacity activeOpacity={0.8}>
                                            <Image source={require('../../../assets/images/login/icon_twitter.png')} style={styles.social} />
                                        </TouchableOpacity>
                                        <TouchableOpacity activeOpacity={0.8}>
                                        <Image source={require('../../../assets/images/login/icon_facebook.png')} style={styles.social} />
                                        </TouchableOpacity>
                                        <TouchableOpacity activeOpacity={0.8}>
                                        <Image source={require('../../../assets/images/login/icon_google.png')} style={styles.social} />
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity activeOpacity={0.3}>
                                        <Text style={stylesDark.textNewAccount}>Create an Account</Text>
                                    </TouchableOpacity>
                                    </View>

                                    <View>
                                    {/* <Text style={{ fontSize: 11, marginTop: 40 }} onPress={() => navigate('Register')}>
                                        Cant have an account? <Text style={{ fontWeight: 'bold', color: '#01aac3' }}>SIGN UP</Text>
                                    </Text> */}
                                    </View>
                                </View>
                                <View style={{ marginTop: 30, width: '50%' }}>
                                    <SwitchSelector
                                        initial={0}
                                        // onPress={value => console.log(`Call onPress with value: ${value}`)}
                                        onPress={(value) => this._changeSwitch(value)}
                                        textColor='black'
                                        selectedColor='white'
                                        buttonColor='black'
                                        borderColor='#01aac3'
                                        hasPadding
                                        options={[
                                            { label: "Light", value: "l" }, //images.feminino = require('./path_to/assets/img/feminino.png')
                                            { label: "Dark", value: "d" } //images.masculino = require('./path_to/assets/img/masculino.png')
                                        ]}
                                    />
                                </View>
                            </View>
                            </ImageBackground>
                        </View>
                        </KeyboardAvoidingView>
                    </View>
                    ) : (
                        <View>
                            <KeyboardAvoidingView behavior="padding">
                            <View>
                                <ImageBackground
                                    style={{ width, height }}
                                    resizeMode="cover"
                                    source={require('../../../assets/images/login/background-login.jpg')}
                                >
                                <View style={styles.container}>
                                    {/* <Image style={styles.logo} source={require('../../../assets/images/login/background-login.jpg')} /> */}

                                    {/* {this.state.isloading ? (
                                    <View>
                                        <ActivityIndicator size="large" style={styles.loading} color="black"/>
                                    </View>
                                    ) : ( */}
                                    <View style={styles.form}>
                                        <View style={styles.formWrapper}>
                                        <TextInput
                                            placeholder="Username"
                                            underlineColorAndroid="transparent"
                                            placeholderTextColor="black"
                                            style={styles.input}
                                            value={username}
                                            keyboardType="email-address"
                                            autoCapitalize="none"
                                            onChangeText={val => this.setState({ username: val })}
                                        />
                                        <TextInput
                                            placeholder="Password"
                                            underlineColorAndroid="transparent"
                                            placeholderTextColor="black"
                                            style={styles.input}
                                            value={password}
                                            secureTextEntry={true}
                                            onChangeText={val => this.setState({ password: val })}
                                        />
                                        </View>

                                        <GradientButton
                                        containerStyle={{ marginTop: 40 }}
                                        textStyle={{ backgroundColor: '#fff', color: '#01aac3', fontSize: 15 }}
                                        text="LOGIN"
                                        onPress={() => this.logIn()}
                                        />

                                        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                        <Text style={styles.socialHeading}>OR SIGN IN WITH:</Text>

                                        <View style={styles.socialContainer}>
                                            <TouchableOpacity activeOpacity={0.8}>
                                                <Image source={require('../../../assets/images/login/icon_twitter.png')} style={styles.social} />
                                            </TouchableOpacity>
                                            <TouchableOpacity activeOpacity={0.8}>
                                            <Image source={require('../../../assets/images/login/icon_facebook.png')} style={styles.social} />
                                            </TouchableOpacity>
                                            <TouchableOpacity activeOpacity={0.8}>
                                            <Image source={require('../../../assets/images/login/icon_google.png')} style={styles.social} />
                                            </TouchableOpacity>
                                        </View>
                                        <TouchableOpacity activeOpacity={0.3}>
                                            <Text style={styles.textNewAccount}>Create an Account</Text>
                                        </TouchableOpacity>
                                        </View>

                                        <View>
                                        {/* <Text style={{ fontSize: 11, marginTop: 40 }} onPress={() => navigate('Register')}>
                                            Cant have an account? <Text style={{ fontWeight: 'bold', color: '#01aac3' }}>SIGN UP</Text>
                                        </Text> */}
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 30, width: '50%' }}>
                                        <SwitchSelector
                                            initial={0}
                                            // onPress={value => console.log(`Call onPress with value: ${value}`)}
                                            onPress={(value) => this._changeSwitch(value)}
                                            textColor='black'
                                            selectedColor='white'
                                            buttonColor='black'
                                            borderColor='#01aac3'
                                            hasPadding
                                            options={[
                                                { label: "Light", value: "l" }, //images.feminino = require('./path_to/assets/img/feminino.png')
                                                { label: "Dark", value: "d" } //images.masculino = require('./path_to/assets/img/masculino.png')
                                            ]}
                                        />
                                    </View>
                                </View>
                                </ImageBackground>
                            </View>
                            </KeyboardAvoidingView>
                        </View>
                    )}
            </View>
        )
    }
}

export default Login;