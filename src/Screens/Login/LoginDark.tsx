// Import Libary - React native
import React, { Component } from 'react'
import {
    ImageBackground,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    KeyboardAvoidingView,
    Dimensions,
  } from 'react-native';

import { GradientButton } from '../../Components';

// Import Libary 3rd
import LinearGradient from 'react-native-linear-gradient';

// Styles Application
import HeaderStyles from "../../Utils/HeaderStyles/HeaderStyles";
import { ColorApp, BackgroundApp, FontApp, FontSizeTextApp } from '../../Utils/Styles/styles';
import styles from './LoginStyles';
import stylesDark from './LoginStylesDark';

import { RootState, UserState } from '../../Reducers';
import { NavigationOptions, ComponentProps } from '../../Configs/ Interfaces';
import {showInfoMessage, showErrorMessage} from '../../Utils/showErrors';
import authService from '../../Services/AuthService';
import { Switch } from 'react-native-switch';

interface Props extends ComponentProps {
    updateUser(data);
    loadUser();
    user: UserState;
}

interface State {
    username: string;
    password: string;
    isloading: boolean;
    switch: boolean;
}
  

class Login extends React.Component<Props, State> {
    // static navigationOptions = {
    //     title: 'Login',
    //     ...HeaderStyles,
    // };

    constructor(props) {
        super(props);
        this.state = {
          username: '',
          password: '',
          isloading: false,
          switch: true,
        };
    }

    logIn() {
        const { username, password } = this.state;
        // if (!username || !password) {
        //     return showInfoMessage('Please fill in all fields');
        // }

        this.setState({ isloading: true });

        // setTimeout(() => {
            this.props.navigation.navigate('Home');
        // }, 2000);
        

        // authService.signInWithEmailPassword({ email, password }).catch(err => {
        // console.log('err', err);
        // this.setState({ isloading: false });

        // if (err.message) {
        //     showErrorMessage(err.message);
        // }
        // });
    }

    _changeSwitch(val) {
        // this.setState({switch: val})
        if (val == false) {
            // redirect screen light
            this.props.navigation.navigate('Login');
        }
    }

    render() {
        const { username, password } = this.state;
        const { width, height } = Dimensions.get('screen');
        return (
            <View>
                <KeyboardAvoidingView behavior="padding">
                <View>
                    <ImageBackground
                        style={{ width, height }}
                        resizeMode="cover"
                        source={require('../../../assets/images/login/background-login.jpg')}
                    >
                    <View style={stylesDark.container}>
                        {/* <Image style={styles.logo} source={require('../../../assets/images/login/background-login.jpg')} /> */}

                        {/* {this.state.isloading ? (
                        <View>
                            <ActivityIndicator size="large" style={styles.loading} color="black"/>
                        </View>
                        ) : ( */}
                        <View style={stylesDark.form}>
                            <View style={stylesDark.formWrapper}>
                            <TextInput
                                placeholder="Username"
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                style={stylesDark.input}
                                value={username}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                onChangeText={val => this.setState({ username: val })}
                            />
                            <TextInput
                                placeholder="Password"
                                underlineColorAndroid="transparent"
                                placeholderTextColor="black"
                                style={stylesDark.input}
                                value={password}
                                secureTextEntry={true}
                                onChangeText={val => this.setState({ password: val })}
                            />
                            </View>

                            <GradientButton
                            containerStyle={{ marginTop: 40 }}
                            textStyle={{ backgroundColor: '#fff', color: '#01aac3', fontSize: 15 }}
                            text="LOGIN"
                            onPress={() => this.logIn()}
                            />

                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                            <Text style={stylesDark.socialHeading}>OR SIGN IN WITH:</Text>

                            <View style={stylesDark.socialContainer}>
                                <TouchableOpacity activeOpacity={0.8}>
                                    <Image source={require('../../../assets/images/login/icon_twitter.png')} style={styles.social} />
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={0.8}>
                                <Image source={require('../../../assets/images/login/icon_facebook.png')} style={styles.social} />
                                </TouchableOpacity>
                                <TouchableOpacity activeOpacity={0.8}>
                                <Image source={require('../../../assets/images/login/icon_google.png')} style={styles.social} />
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity activeOpacity={0.3}>
                                <Text style={stylesDark.textNewAccount}>Create an Account</Text>
                            </TouchableOpacity>
                            </View>

                            <View>
                            {/* <Text style={{ fontSize: 11, marginTop: 40 }} onPress={() => navigate('Register')}>
                                Cant have an account? <Text style={{ fontWeight: 'bold', color: '#01aac3' }}>SIGN UP</Text>
                            </Text> */}
                            </View>
                        </View>
                        <View style={{ marginTop: 30 }}>
                                <Switch
                                    value={this.state.switch}
                                    onValueChange={(val) => this._changeSwitch(val)}
                                    disabled={false}
                                    activeText={'On'}
                                    inActiveText={'Off'}
                                    circleSize={30}
                                    barHeight={1}
                                    circleBorderWidth={3}
                                    backgroundActive={'white'}
                                    backgroundInactive={'black'}
                                    circleActiveColor={'white'}
                                    circleInActiveColor={'black'}
                                    changeValueImmediately={true}
                                    // renderInsideCircle={() => <CustomComponent />} // custom component to render inside the Switch circle (Text, Image, etc.)
                                    // changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                                    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                                    outerCircleStyle={{}} // style for outer animated circle
                                    renderActiveText={false}
                                    renderInActiveText={false}
                                    switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                                    switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                                    switchWidthMultiplier={2} // multipled by the `circleSize` prop to calculate total width of the Switch
                                    switchBorderRadius={30} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
                                />
                            </View>
                        {/* )} */}
                    </View>
                    </ImageBackground>
                </View>
                </KeyboardAvoidingView>
            </View>
        )
    }
}

export default Login;