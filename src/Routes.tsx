import React from 'react';
import { Text, View , Image, Button, TouchableOpacity} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import IoniconsFontAwesome from 'react-native-vector-icons/FontAwesome';
import IoniconsSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { createBottomTabNavigator, createAppContainer, BottomTabBar, createStackNavigator } from 'react-navigation';

import HomeScreen from './Screens/Home/Home';
import DetailSlideHomeScreen from './Screens/Home/DetailSlideHome';
import ListResidentialMarketScreen from './Screens/Home/ListResidentialMarket';
import FlastlistResidentialMarketScreen from './Screens/Home/FlastlistResidentialMarket';
import DetailFlastlistResidentialMarketScreen from './Screens/Home/DetailFlastlistResidentialMarket';
import RegisterProductScreen from './Screens/Home/RegisterProduct';

import ServiceScreen from './Screens/Service/Service';
import FlastlistServiceScreen from './Screens/Service/FlastlistService';
import DetailServiceScreen from './Screens/Service/DetailService';
import SendEmailScreen from './Screens/Service/SendEmail';

import EvaluateScreen from './Screens/Evaluate/Evaluate';
import CreateNewEvaluateScreen from './Screens/Evaluate/CreateNewEvaluate';

import NoticeScreen from './Screens/Notice/Notice';
import FlastlistNoticeScreen from './Screens/Notice/FlastlistNotice';
import DetailNoticeScreen from './Screens/Notice/DetailNotice';
import DetailListNoticeScreen from './Screens/Notice/DetailListNotice';
import CreateEvaluateNoticeScreen from './Screens/Notice/CreateEvaluateNotice';
const TabBarComponent = (props) => (<BottomTabBar {...props} />);

// class IconWithBadge extends React.Component {
//   render() {
//     const { name, badgeCount, color, size } = this.props;
//     return (
//       <View style={{ width: 24, height: 24, margin: 5 }}>
//         <Ionicons name={name} size={size} color={color} />
//         {badgeCount > 0 && (
//           <View
//             style={{
//               // /If you're using react-native < 0.57 overflow outside of the parent
//               // will not work on Android, see https://git.io/fhLJ8
//               position: 'absolute',
//               right: -6,
//               top: -3,
//               backgroundColor: 'red',
//               borderRadius: 6,
//               width: 12,
//               height: 12,
//               justifyContent: 'center',
//               alignItems: 'center',
//             }}>
//             <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>
//               {badgeCount}
//             </Text>
//           </View>
//         )}
//       </View>
//     );
//   }
// }

// const HomeIconWithBadge = props => {
//   // You should pass down the badgeCount in some other ways like context, redux, mobx or event emitters.
//   return <IconWithBadge {...props} badgeCount={3} />;
// };

const getTabBarIcon = (navigation, focused, tintColor) => {
    const { routeName } = navigation.state;
    let IconComponent = Ionicons;
    let iconName;
    if (routeName === 'Home') {
        return focused ? <Ionicons name="md-home" size={18} color="#177EFB" /> :
          <Ionicons name="md-home" size={18} color="#73C7FD" />;
    } else if (routeName === 'Service') {
        return focused ? <IoniconsFontAwesome name="gear" size={18} color="#177EFB" /> :
          <IoniconsFontAwesome name="gear" size={18} color="#73C7FD" />;
    } else if (routeName === 'Evaluate') {
        return focused ? <IoniconsSimpleLineIcons name="note" size={18} color="#177EFB" /> :
          <IoniconsSimpleLineIcons name="note" size={18} color="#73C7FD" />;
    } else if (routeName === 'Notice') {
      return focused ? <Ionicons name="md-notifications" size={18} color="#177EFB" /> :
        <Ionicons name="md-notifications" size={18} color="#73C7FD" />;
  }

    // You can return any component that you like here!
    return <IconComponent name={iconName} size={25} color={tintColor} />;
};

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  DetailSlideHome: DetailSlideHomeScreen,
  ListResidentialMarket: ListResidentialMarketScreen,
  FlastlistResidentialMarket: FlastlistResidentialMarketScreen,
  DetailFlastlistResidentialMarket: DetailFlastlistResidentialMarketScreen,
  RegisterProduct: RegisterProductScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Trang chủ',
};

const ServiceStack = createStackNavigator({
  Service: ServiceScreen,
  FlastlistService: FlastlistServiceScreen,
  DetailService: DetailServiceScreen,
  SendEmail: SendEmailScreen
});

ServiceStack.navigationOptions = {
  tabBarLabel: 'Dịch vụ',
};

const EvaluateStack = createStackNavigator({
  Evaluate: EvaluateScreen,
  CreateNewEvaluate: CreateNewEvaluateScreen
});

EvaluateStack.navigationOptions = {
  tabBarLabel: 'Đánh giá',
};

const NoticeStack = createStackNavigator({
  Notice: NoticeScreen,
  FlastlistNotice: FlastlistNoticeScreen,
  DetailNotice: DetailNoticeScreen,
  DetailListNotice: DetailListNoticeScreen,
  CreateEvaluateNotice: CreateEvaluateNoticeScreen
});

NoticeStack.navigationOptions = {
  tabBarLabel: 'Thông báo',
};


export default createAppContainer(
  createBottomTabNavigator(
    {
      Home: { screen: HomeStack },
      Service: { screen: ServiceStack },
      Evaluate: { screen: EvaluateStack },
      Notice: { screen: NoticeStack },
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) =>
          getTabBarIcon(navigation, focused, tintColor),
      }),
      tabBarComponent: props =>
      <TabBarComponent
        {...props}
        style={{
          shadowColor: '#9d9d9d',
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.5,
          shadowRadius: 4,
          height:50,
          paddingTop:8,
        }}
      />,
      tabBarOptions: {
        activeTintColor: '#177EFB',
        inactiveTintColor: '#177EFB',
        //activeBackgroundColor:'green',
        // activeLineBottom:'#D40D12',
        labelStyle: {
          marginBottom: 8,
          fontSize: 12
        },
      },
    }
  )
);
