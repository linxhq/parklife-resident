export const USER_LOGIN = '[User] Login';

export function userLogin(payload) {
  return { type: USER_LOGIN, payload };
}
